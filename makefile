
FILES_GAMES = \
	obj/graphic/BulletView.o \
	obj/graphic/TurretView.o \
	obj/graphic/ChunkView.o \
	obj/graphic/PolygoneView.o \
	obj/graphic/RectangleView.o \
	obj/graphic/GameManager.o \
	obj/graphic/TileView.o \
	obj/graphic/WorldView.o \
	obj/map_data/ChunkData.o \
	obj/map_data/WorldData.o \
	obj/utils/utils.o 

FILES_UTILS = \
	obj/utils/TimeAnimation.o


FILES_WORLD_MAP_GENERATOR = \
	obj/map_generator/MGChunk.o \
	obj/map_generator/MGWorld.o \
	obj/map_generator/MGLayerConfig.o


COMPILER_OPTION = -g -Wall

SFML_OPTION = -lsfml-graphics -lsfml-system -lsfml-window

build : bin/games

buildobjdir :
	mkdir obj
	mkdir obj/graphic
	mkdir obj/map_data
	mkdir obj/map_generator
	mkdir obj/utils

clean : 
	rm obj/graphic.o obj/test_collision_map.o obj/test_edge_map_generate.o obj/test_edge_tile.o obj/test_optimised_map.o obj/test_infinite_map $(FILES_GAMES)

bin/games : $(FILES_GAMES) $(FILES_WORLD_MAP_GENERATOR) $(FILES_UTILS) obj/games.o
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ $^

bin/test_infinite_map : $(FILES_GAMES) $(FILES_WORLD_MAP_GENERATOR) obj/test_infinite_map.o
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ $^

bin/test_optimised_map : $(FILES_GAMES) $(FILES_WORLD_MAP_GENERATOR) obj/test_optimised_map.o
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ $^

bin/test_edge_map_generate : $(FILES_GAMES) $(FILES_WORLD_MAP_GENERATOR) obj/test_edge_map_generate.o
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ $^

bin/test_collision_map : $(FILES_GAMES) $(FILES_WORLD_MAP_GENERATOR) $(FILES_UTILS) obj/test_collision_map.o
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ $^ 

bin/test_edge_tile : $(FILES_GAMES) $(FILES_WORLD_MAP_GENERATOR) obj/test_edge_tile.o
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ $^ 



obj/graphic/%.o : src/graphic/%.cpp src/graphic/%.hpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/map_data/%.o : src/map_data/%.cpp src/map_data/%.hpp
	g++ $(COMPILER_OPTION) -o $@ -c $<

obj/map_generator/%.o : src/map_generator/%.cpp src/map_generator/%.hpp
	g++ $(COMPILER_OPTION) -o $@ -c $<

obj/utils/%.o : src/utils/%.cpp src/utils/%.hpp
	g++ $(COMPILER_OPTION) -o $@ -c $<


obj/games.o : src/games.cpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/utils/utils.o : src/utils/utils.cpp src/utils/utils.hpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/test_infinite_map.o : src/test_infinite_map.cpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/test_optimised_map.o : src/test_optimised_map.cpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/test_edge_map_generate.o : src/test_edge_map_generate.cpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/test_collision_map.o : src/test_collision_map.cpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 

obj/test_edge_tile.o : src/test_edge_tile.cpp
	g++ $(COMPILER_OPTION) $(SFML_OPTION) -o $@ -c $< 