#ifndef WORLD_VIEW_H
#define WORLD_VIEW_H

#include <SFML/Graphics.hpp>

#include <map>
class ChunkView;
class WorldData;

class WorldView : public sf::Drawable
{
	public:
		WorldView(WorldData *worldData, float pValue);
		~WorldView();

		void setPValue(float pValue);
		void setView(sf::View &view);
		void updateRender();
		
	protected:
		
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		
	private:
		ChunkView *renderChunkIfNotExist(int x, int y);
		void addPointValue(std::map<std::string, ChunkView*> &chunkToRender, sf::Vector2i position, float addValue);
		std::string chunkKey(int x, int y);

		std::map<std::string, ChunkView*> m_chunks;
		std::vector<ChunkView *> m_chunksToDraw;
		float m_pValue;

		WorldData *m_worldData;
};

#endif
