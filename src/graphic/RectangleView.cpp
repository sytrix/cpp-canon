#include "RectangleView.hpp"



RectangleView::RectangleView() 
: PolygoneView()
{
    this->resize(4);
}

RectangleView::~RectangleView() {
	
}

void RectangleView::setColor(sf::Color color)
{
    for (int i = 0; i < 4; ++i) {
        (*this)[i].color = color;
    }
}

void RectangleView::setPosition(sf::Vector2f position)
{
    m_position = position;
    this->updateShape();
}

void RectangleView::setSize(sf::Vector2f size) 
{
    m_size = size;
    this->updateShape();
}

void RectangleView::updateShape() 
{
    (*this)[0].position = m_position;
    (*this)[1].position = sf::Vector2f(m_position.x, m_position.y + m_size.y);
    (*this)[2].position = sf::Vector2f(m_position.x + m_size.x, m_position.y + m_size.y);
    (*this)[3].position = sf::Vector2f(m_position.x + m_size.x, m_position.y);
}
