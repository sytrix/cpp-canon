#ifndef POLYGONE_VIEW_H
#define POLYGONE_VIEW_H

#include <SFML/Graphics.hpp>

class PolygoneView : public sf::VertexArray
{
	public:
		PolygoneView();
		~PolygoneView();

		sf::Vector2f collisionPoint(sf::Vector2f pointA, sf::Vector2f pointB);
		
	protected:
		
	private:
};

#endif
