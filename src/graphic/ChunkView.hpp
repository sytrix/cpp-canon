#ifndef CHUNK_VIEW_H
#define CHUNK_VIEW_H

#include <SFML/Graphics.hpp>
class ChunkData;

class ChunkView : public sf::Drawable
{
	public:
		ChunkView(ChunkData *chunkData, sf::Vector2i chunkPos);
		~ChunkView();

		sf::Vector2i getChunkPosition();
		void render(float pValue);
		
	protected:
		
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	private:

		void makeTile(
			float posX, float posY, 
			float vUL, float vUR, float vDR, float vDL, 
			float p, 
			sf::Vertex *vertexArray, 
			int vertexIndex, 
			int nbVertex, 
			unsigned char code);

		ChunkData *m_chunkData;
		sf::VertexBuffer m_vertexBuffer;
		sf::Vector2i m_chunkPosition;
		
};

#endif
