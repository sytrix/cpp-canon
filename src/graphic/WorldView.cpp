#include "WorldView.hpp"

#include <cmath>
#include <cfloat>
#if defined(WIN32) || defined(_WIN64)
#define _USE_MATH_DEFINES
#include "math.h"
#endif
#include <iostream>
#include <cfloat>

#include "ChunkView.hpp"

#include "../map_generator/MGWorld.hpp"
#include "../map_generator/MGChunk.hpp"
#include "../map_generator/MGLayerConfig.hpp"

#include "../map_data/WorldData.hpp"

#include "../utils/utils.hpp"
#include "../const.hpp"

WorldView::WorldView(WorldData *worldData, float pValue) 
: m_pValue(pValue), m_worldData(worldData)
{

}

WorldView::~WorldView() 
{
    for (std::map<std::string, ChunkView*>::iterator itr = m_chunks.begin(); itr != m_chunks.end(); ++itr) { 
        delete itr->second;
    }
}

void WorldView::setPValue(float pValue)
{
    m_pValue = pValue;

    for (std::map<std::string, ChunkView*>::iterator itr = m_chunks.begin(); itr != m_chunks.end(); ++itr) { 
        ChunkView *gameChunk = itr->second;
        gameChunk->render(m_pValue);
    }
}

void WorldView::setView(sf::View &view)
{
    m_chunksToDraw.clear();

    sf::Vector2f center = view.getCenter();
    sf::Vector2f size = view.getSize();

    float viewOriX = center.x - size.x / 2.f;
    float viewOriY = center.y - size.y / 2.f;
    float viewEndX = center.x + size.x / 2.f;
    float viewEndY = center.y + size.y / 2.f;
    int indexOriX = viewOriX / TILE_SIZE / CHUNK_SIZE;
    int indexOriY = viewOriY / TILE_SIZE / CHUNK_SIZE;
    int indexEndX = viewEndX / TILE_SIZE / CHUNK_SIZE;
    int indexEndY = viewEndY / TILE_SIZE / CHUNK_SIZE;

    if (viewOriX < 0.f) {
        indexOriX--;
    }
    if (viewOriY < 0.f) {
        indexOriY--;
    }

    for (int x = indexOriX; x < indexEndX + 1; ++x) {
        for (int y = indexOriY; y < indexEndY + 1; ++y) {
            ChunkView *chunk = this->renderChunkIfNotExist(x, y);
            m_chunksToDraw.push_back(chunk);
        }
    }
}

void WorldView::updateRender()
{
    for (ChunkView *chunkView : m_chunksToDraw) {
        chunkView->render(m_pValue);
    }
}

void WorldView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    //for (std::vector<ChunkView*>::const_iterator itr = m_chunksToDraw.begin(); itr != m_chunksToDraw.end(); ++itr) {
    for (ChunkView *chunk : m_chunksToDraw) {
        target.draw(*chunk, states);
    }
}

ChunkView *WorldView::renderChunkIfNotExist(int x, int y)
{
    //sf::Clock clock;
    std::string chunkKey = this->chunkKey(x, y);
    std::map<std::string, ChunkView *>::iterator it = m_chunks.find(chunkKey);
    ChunkView *chunkView = nullptr;

    

    if (it == m_chunks.end()) {
        ChunkData *chunkData = m_worldData->getChunkOrGenerateIt(x, y);
        chunkView = new ChunkView(chunkData, sf::Vector2i(x, y));
        m_chunks[chunkKey] = chunkView;
    } else {
        chunkView = it->second;
    }

    return chunkView;
}

std::string WorldView::chunkKey(int x, int y)
{
    return std::to_string(x) + ";" + std::to_string(y);
}


