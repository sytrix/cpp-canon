#include "BulletView.hpp"

#include <cmath>

#define BULLET_RADIUS 5.f

BulletView::BulletView(TurretView *parent, sf::Vector2f position, float speed, float angle) 
: m_parent(parent), m_isAlive(true)
{
    m_circleShape.setPosition(position);
	m_circleShape.setFillColor(sf::Color::White);
    m_circleShape.setRadius(BULLET_RADIUS);
    m_circleShape.setOrigin(BULLET_RADIUS, BULLET_RADIUS);

    m_velocity.x = cos(angle) * speed;
    m_velocity.y = sin(angle) * speed;
}

BulletView::~BulletView() 
{
	
}

TurretView *BulletView::getParent()
{
    return m_parent;
}

sf::Vector2f BulletView::getPosition()
{
    return m_circleShape.getPosition();
}

sf::Vector2f BulletView::getVelocity()
{
    return m_velocity;
}

bool BulletView::isAlive()
{
    return m_isAlive;
}

void BulletView::updatePosition()
{
    m_circleShape.move(m_velocity);
}

void BulletView::terminate()
{
    m_isAlive = false;
}

void BulletView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if (m_isAlive) {
        target.draw(m_circleShape, states);
    }
}