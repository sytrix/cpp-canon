#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <SFML/Graphics.hpp>
#include <vector>
class BulletView;
class TurretView;
class PolygoneView;
class WorldView;
class WorldData;

class GameManager : public sf::Drawable
{
	public:
		GameManager(std::string seed, float pValue);
		~GameManager();

		void addBullet(BulletView *bullet);
		void addCanon(TurretView *canon);
		void addPolygone(PolygoneView *poly);

		void update2();
		void update();
		void fire();

		void brush(sf::Vector2f position, float radius, float addValue);
		sf::Vector2f getCollisionPoint(sf::Vector2f pointA, sf::Vector2f pointB);
		sf::Vector2f getNearestPoint(sf::Vector2f point, float radius);
		bool isInsideWall(sf::Vector2f point);

		void setView(sf::View &view);
		void setMousePosition(sf::Vector2f position);
		
	protected:

		void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		
	private:
		std::vector<BulletView *> m_bullets;
		std::vector<TurretView *> m_canons;
		std::vector<PolygoneView *> m_polygones;

		WorldView *m_worldView;
		WorldData *m_worldData;

		sf::View *m_view;
		
		sf::Vector2f m_mousePosition;
};

#endif
