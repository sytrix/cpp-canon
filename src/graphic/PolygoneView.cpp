#include "PolygoneView.hpp"

#include <cmath>
#include <iostream>

#include "../utils/utils.hpp"

PolygoneView::PolygoneView() 
: sf::VertexArray(sf::PrimitiveType::TriangleFan)
{
	
}

PolygoneView::~PolygoneView() {
	
}

sf::Vector2f PolygoneView::collisionPoint(sf::Vector2f pointA, sf::Vector2f pointB)
{
    int nbVertex = this->getVertexCount();

    for (int i = 0; i < nbVertex; i++) {

        sf::Vector2f polyPointA = (*this)[i].position;
        sf::Vector2f polyPointB;

        if (i == nbVertex -1) {
            polyPointB = (*this)[0].position;
        } else {
            polyPointB = (*this)[i+1].position;
        }

        sf::Vector3f cartA = equCart(pointA, pointB);
		sf::Vector3f cartB = equCart(polyPointA, polyPointB);

        sf::Vector2f pointInter = intersectionPoint(cartA, cartB);

        if (!isnanf(pointInter.x) && !isnanf(pointInter.y)) {
            if (checkPointBetween(pointInter, pointA, pointB, polyPointA, polyPointB)) {
                return pointInter;
            }
        }

    }

    return sf::Vector2f(NAN, NAN);
}