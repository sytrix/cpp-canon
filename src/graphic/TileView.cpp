#include "TileView.hpp"

#include <iostream>
#include "../utils/utils.hpp"

#define TILE_SIZE 40.f


TileView::TileView(int posX, int posY, float vUL, float vUR, float vDR, float vDL, float p) 
: m_vertexArray(sf::PrimitiveType::Triangles)
{
	unsigned char code = getCode(vUL, vUR, vDR, vDL, p);
    int nbPoint = getNbPoint(code);
    
    //std::cout << "code:" << (int)code << std::endl;

    m_vertexArray.resize(nbPoint);

    const char *points = vertexMatrix[code];

    float valueA = getValueAWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;
    float valueB = getValueBWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;
    float valueC = getValueCWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;
    float valueD = getValueDWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;

    // valueA = TILE_SIZE / 2.f;
    // valueB = TILE_SIZE / 2.f;

    float average = (vUL + vUR + vDR + vDL) / 4.f;
    average *= 0.8f;

    if (average > 1.f) {
        average = 1.f;
    }
    

    for (int i = 0; i < nbPoint; i++) {
        int ptX = points[i * 2];
        int ptY = points[i * 2 + 1];
        float valueX;
        float valueY;

        if (ptX == 1) {
            valueX = posX * TILE_SIZE + TILE_SIZE;
        } else if (ptX == 2) {
            valueX = posX * TILE_SIZE + valueA;
        } else if (ptX == 3) {
            valueX = posX * TILE_SIZE + valueB;
        } else if (ptX == 4) {
            valueX = posX * TILE_SIZE + valueC;
        } else if (ptX == 5) {
            valueX = posX * TILE_SIZE + valueD;
        } else {
            valueX = posX * TILE_SIZE;
        }

        if (ptY == 1) {
            valueY = posY * TILE_SIZE + TILE_SIZE;
        } else if (ptY == 2) {
            valueY = posY * TILE_SIZE + valueA;
        } else if (ptY == 3) {
            valueY = posY * TILE_SIZE + valueB;
        } else if (ptY == 4) {
            valueY = posY * TILE_SIZE + valueC;
        } else if (ptY == 5) {
            valueY = posY * TILE_SIZE + valueD;
        } else {
            valueY = posY * TILE_SIZE;
        }

        m_vertexArray[i].position = sf::Vector2f(valueX, valueY);
        
        m_vertexArray[i].color = sf::Color(60 * average, 240 * average, 80 * average);
    }


}

TileView::~TileView() 
{
	
}

void TileView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    //states.transform = sf::Transform().translate(sf::Vector2f(50.f, 50.f));
    target.draw(m_vertexArray, states);
}

