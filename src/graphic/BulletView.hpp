#ifndef BULLET_VIEW_H
#define BULLET_VIEW_H

#include <SFML/Graphics.hpp>
class TurretView;

class BulletView : public sf::Drawable
{
	public:
		BulletView(TurretView *parent, sf::Vector2f position, float speed, float angle);
		~BulletView();

		TurretView *getParent();
		sf::Vector2f getPosition();
		sf::Vector2f getVelocity();
		bool isAlive();

		void updatePosition();
		void terminate();
		
	protected:
	
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		
	private:
		TurretView *m_parent;
		sf::CircleShape m_circleShape;
		sf::Vector2f m_velocity;
		bool m_isAlive;
		
		

};

#endif
