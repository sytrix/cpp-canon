#include "GameManager.hpp"

#include <iostream>
#include <cmath>

#include "BulletView.hpp"
#include "TurretView.hpp"
#include "PolygoneView.hpp"
#include "WorldView.hpp"

#include "../map_data/WorldData.hpp"

#include "../utils/utils.hpp"


GameManager::GameManager(std::string seed, float pValue) 
: m_view(nullptr)
{
    m_worldData = new WorldData("bob", pValue);
	m_worldView = new WorldView(m_worldData, pValue);
}

GameManager::~GameManager() 
{

    for (std::vector<PolygoneView *>::iterator it = m_polygones.begin(); it != m_polygones.end(); ++it) {
        delete *it;
    }
    m_polygones.clear();

    for (std::vector<BulletView *>::iterator it = m_bullets.begin(); it != m_bullets.end(); ++it) {
        delete *it;
    }
    m_bullets.clear();

    for (std::vector<TurretView *>::iterator it = m_canons.begin(); it != m_canons.end(); ++it) {
        delete *it;
    }

    delete m_worldView;
    delete m_worldData;

    m_canons.clear();

}

void GameManager::addBullet(BulletView *bullet)
{
    m_bullets.push_back(bullet);
}

void GameManager::addCanon(TurretView *canon)
{
    m_canons.push_back(canon);
}

void GameManager::addPolygone(PolygoneView *poly)
{
    m_polygones.push_back(poly);
}

void GameManager::update2()
{
    for (std::vector<BulletView *>::iterator it = m_bullets.begin(); it != m_bullets.end(); ++it) {
        BulletView *bullet = *it;

        bool collision = false;

        if (bullet->isAlive()) {

            sf::Vector2f collisionPoint = m_worldData->getCollisionPoint(bullet->getPosition(), bullet->getPosition() + bullet->getVelocity());
            if (!isnanf(collisionPoint.x) && !isnanf(collisionPoint.y)) {
                bullet->getParent()->setPosition(collisionPoint);
                bullet->terminate();
                collision = true;
                break;
            }
        }

        if (!collision) {
            bullet->updatePosition();
        }
    }

    std::vector<BulletView *>::iterator bulletIterator = m_bullets.begin();
    while(bulletIterator != m_bullets.end()) {
        BulletView *bullet = *bulletIterator;
        //bullet->updatePosition();
        if (bullet->isAlive()) {
            ++bulletIterator;
        } else {
            delete bullet;
            bulletIterator = m_bullets.erase(bulletIterator);
        }
    }
}

void GameManager::update()
{
    for (std::vector<BulletView *>::iterator it = m_bullets.begin(); it != m_bullets.end(); ++it) {
        BulletView *bullet = *it;

        bool collision = false;

        if (bullet->isAlive()) {

            for (std::vector<PolygoneView *>::const_iterator it = m_polygones.begin(); it != m_polygones.end(); ++it) {
                PolygoneView *poly = *it;
                
                sf::Vector2f collisionPoint = poly->collisionPoint(bullet->getPosition(), bullet->getPosition() + bullet->getVelocity());
                if (!isnanf(collisionPoint.x) && !isnanf(collisionPoint.y)) {
                    //rectangle->setColor(sf::Color(rand() % 192 + 64, rand() % 192 + 64, rand() % 192 + 64));
                    bullet->getParent()->setPosition(collisionPoint);
                    bullet->terminate();
                    collision = true;
                    break;
                }
            }
        }

        if (!collision) {
            bullet->updatePosition();
        }
    }

    std::vector<BulletView *>::iterator bulletIterator = m_bullets.begin();
    while(bulletIterator != m_bullets.end()) {
        BulletView *bullet = *bulletIterator;
        //bullet->updatePosition();
        if (bullet->isAlive()) {
            ++bulletIterator;
        } else {
            delete bullet;
            bulletIterator = m_bullets.erase(bulletIterator);
        }
    }
}

void GameManager::fire() 
{
    for (std::vector<TurretView *>::iterator it = m_canons.begin(); it != m_canons.end(); ++it) {
        TurretView *canon = *it;
        this->addBullet(canon->fire());
    }
}

void GameManager::brush(sf::Vector2f position, float radius, float addValue)
{

}

sf::Vector2f GameManager::getCollisionPoint(sf::Vector2f pointA, sf::Vector2f pointB)
{
    return sf::Vector2f();
}

sf::Vector2f GameManager::getNearestPoint(sf::Vector2f point, float radius)
{
    return sf::Vector2f();
}

bool GameManager::isInsideWall(sf::Vector2f point)
{
    return true;
}

void GameManager::setView(sf::View &view)
{
    //m_view = &view;
    m_worldView->setView(view);

    m_worldView->updateRender();
}

void GameManager::setMousePosition(sf::Vector2f position)
{
    m_mousePosition = position;

    for (std::vector<TurretView *>::iterator it = m_canons.begin(); it != m_canons.end(); ++it) {
        TurretView *canon = *it;
        canon->lookAt(m_mousePosition);
    }
}

void GameManager::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    target.draw(*m_worldView, states);

    for (std::vector<PolygoneView *>::const_iterator it = m_polygones.begin(); it != m_polygones.end(); ++it) {
        PolygoneView *polygone = *it;
        target.draw(*polygone, states);
    }

    for (std::vector<BulletView *>::const_iterator it = m_bullets.begin(); it != m_bullets.end(); ++it) {
        BulletView *bullet = *it;
        target.draw(*bullet, states);
    }

    for (std::vector<TurretView *>::const_iterator it = m_canons.begin(); it != m_canons.end(); ++it) {
        TurretView *canon = *it;
        target.draw(*canon, states);
    }
}

