#include "TurretView.hpp"

#include <iostream>
#include <cmath>

#if defined(WIN32) || defined(_WIN64)
#define _USE_MATH_DEFINES
#include "math.h"
#endif

#include "BulletView.hpp"

#define CANON_BODY_RADIUS   20.f
#define CANON_TUBE_W        10.f
#define CANON_TUBE_H        40.f

TurretView::TurretView()
	: m_canonShape(sf::Vector2f(CANON_TUBE_W, CANON_TUBE_H)), m_angle(0.0), m_show(true)
{
	m_bodyShape.setFillColor(sf::Color(192, 192, 192));
    m_bodyShape.setRadius(CANON_BODY_RADIUS);
    m_bodyShape.setOrigin(CANON_BODY_RADIUS, CANON_BODY_RADIUS);

    m_canonShape.setFillColor(sf::Color(192, 192, 192));
    m_canonShape.setOrigin(CANON_TUBE_W / 2.f, 0.f);

}

TurretView::~TurretView() {
	
}

void TurretView::setPosition(sf::Vector2f position)
{
    m_bodyShape.setPosition(position);
    m_canonShape.setPosition(position);
}

void TurretView::setRotation(float angle)
{
    m_angle = angle;
    m_canonShape.setRotation((m_angle * 180.0 / M_PI) - 90.0);
}

void TurretView::lookAt(sf::Vector2f position)
{
    sf::Vector2f objPos = m_bodyShape.getPosition();
    this->setRotation(atan2(position.y - objPos.y, position.x - objPos.x));
}

void TurretView::show() 
{
    m_show = true;
}

void TurretView::hide() 
{
    m_show = false;
}

BulletView *TurretView::fire()
{
    return new BulletView(this, this->canonExtremity(), 25.5f, m_angle);
}

sf::Vector2f TurretView::getPosition()
{
    return m_bodyShape.getPosition();
}

void TurretView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    if (m_show) {
        target.draw(m_bodyShape, states);
        target.draw(m_canonShape, states);
    }
}

sf::Vector2f TurretView::canonExtremity()
{
    sf::Vector2f result = m_bodyShape.getPosition();

    result.x += cos(m_angle) * CANON_TUBE_H;
    result.y += sin(m_angle) * CANON_TUBE_H;

    return result;
}