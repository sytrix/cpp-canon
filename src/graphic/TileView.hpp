#ifndef TILE_VIEW_H
#define TILE_VIEW_H

#include <SFML/Graphics.hpp>

class TileView : public sf::Drawable
{
	public:
		TileView(int posX, int posY, float vUL, float vUR, float vDR, float vDL, float p);
		~TileView();
		
	protected:
		
		void draw(sf::RenderTarget& target, sf::RenderStates states) const;

	private:
		sf::VertexArray m_vertexArray;
};

#endif
