#ifndef TURRET_VIEW_H
#define TURRET_VIEW_H

#include <SFML/Graphics.hpp>
class BulletView;

class TurretView : public sf::Drawable
{
	public:
		TurretView();
		~TurretView();

		void setPosition(sf::Vector2f position);
		void setRotation(float angle);
		void lookAt(sf::Vector2f position);

		void hide();
		void show();

		BulletView *fire();

		sf::Vector2f getPosition();
		
	protected:

		void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		
	private:

		sf::Vector2f canonExtremity();
		
		sf::CircleShape m_bodyShape;
		sf::RectangleShape m_canonShape;
		double m_angle;
		bool m_show;
};

#endif
