#ifndef RECTANGLE_VIEW_H
#define RECTANGLE_VIEW_H

#include <SFML/Graphics.hpp>
#include "PolygoneView.hpp"

class RectangleView : public PolygoneView
{
	public:
		RectangleView();
		~RectangleView();

		void setPosition(sf::Vector2f position);
		void setSize(sf::Vector2f size);
		void setColor(sf::Color color);

		bool isContainingPoint(sf::Vector2f point);

		void updateShape();
		
	protected:
		

	private:
		sf::Vector2f m_position;
		sf::Vector2f m_size;
};

#endif
