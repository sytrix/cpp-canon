#include "ChunkView.hpp"

#include <iostream>
#include <cmath>

#include "../map_data/ChunkData.hpp"

#include "../const.hpp"
#include "../utils/utils.hpp"


ChunkView::ChunkView(ChunkData *chunkData, sf::Vector2i chunkPos) 
: 
    m_chunkData(chunkData), 
    m_vertexBuffer(sf::PrimitiveType::Triangles), 
    m_chunkPosition(chunkPos)
{
    
}

ChunkView::~ChunkView() 
{
}

sf::Vector2i ChunkView::getChunkPosition()
{
    return m_chunkPosition;
}

void ChunkView::render(float pValue)
{
    if (m_chunkData->isRender()) {
        return;
    }

    float *mapData = m_chunkData->getData();

    int countNbVertex = 0;
    for (int x = 0; x < CHUNK_SIZE; ++x) {
        for (int y = 0; y < CHUNK_SIZE; ++y) {
            float vUL = mapData[y * (CHUNK_SIZE + 1) + x];
            float vDL = mapData[(y + 1) * (CHUNK_SIZE + 1) + x];
            float vDR = mapData[(y + 1) * (CHUNK_SIZE + 1) + (x + 1)];
            float vUR = mapData[y * (CHUNK_SIZE + 1) + (x + 1)];
            unsigned char code = getCode(vUL, vUR, vDR, vDL, pValue);
            countNbVertex += getNbPoint(code);
        }   
    }
    
    sf::Vertex *vertexArray = new sf::Vertex[countNbVertex];
    
    int vertexIndex = 0;
    float chunkRealPosX = m_chunkPosition.x * CHUNK_SIZE * TILE_SIZE;
    float chunkRealPosY = m_chunkPosition.y * CHUNK_SIZE * TILE_SIZE;
    for (int x = 0; x < CHUNK_SIZE; ++x) {
        for (int y = 0; y < CHUNK_SIZE; ++y) {
            float vUL = mapData[y * (CHUNK_SIZE + 1) + x];
            float vDL = mapData[(y + 1) * (CHUNK_SIZE + 1) + x];
            float vDR = mapData[(y + 1) * (CHUNK_SIZE + 1) + (x + 1)];
            float vUR = mapData[y * (CHUNK_SIZE + 1) + (x + 1)];
            unsigned char code = getCode(vUL, vUR, vDR, vDL, pValue);
            int nbPoint = getNbPoint(code);
            float tileRealPosX = chunkRealPosX + x * TILE_SIZE;
            float tileRealPosY = chunkRealPosY + y * TILE_SIZE;
            this->makeTile(tileRealPosX, tileRealPosY, vUL, vUR, vDR, vDL, pValue, vertexArray, vertexIndex, nbPoint, code);
            vertexIndex += nbPoint;
        }
    }

    m_vertexBuffer.create(countNbVertex);
    m_vertexBuffer.update(vertexArray);

    m_chunkData->render();
        
    delete[] vertexArray;
}

void ChunkView::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
    //states.transform = sf::Transform().translate(sf::Vector2f(50.f, 50.f));
    target.draw(m_vertexBuffer, states);
}

void ChunkView::makeTile(
    float posX, float posY, 
    float vUL, float vUR, float vDR, float vDL, 
    float p, 
    sf::Vertex *vertexArray, 
    int vertexIndex, 
    int nbVertex, 
    unsigned char code)
{
    const char *points = vertexMatrix[code];

    float valueA = getValueAWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;
    float valueB = getValueBWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;
    float valueC = getValueCWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;
    float valueD = getValueDWithCode(code, vUL, vUR, vDR, vDL, p) * TILE_SIZE;

    // valueA = TILE_SIZE / 2.f;
    // valueB = TILE_SIZE / 2.f;

    float average = (vUL + vUR + vDR + vDL) / 4.f;
    average *= 0.8f;

    if (average > 1.f) {
        average = 1.f;
    }
    

    for (int i = 0; i < nbVertex; i++) {
        int ptX = points[i * 2];
        int ptY = points[i * 2 + 1];
        float valueX;
        float valueY;

        if (ptX == 1) {
            valueX = posX + TILE_SIZE;
        } else if (ptX == 2) {
            valueX = posX + valueA;
        } else if (ptX == 3) {
            valueX = posX + valueB;
        } else if (ptX == 4) {
            valueX = posX + valueC;
        } else if (ptX == 5) {
            valueX = posX + valueD;
        } else {
            valueX = posX;
        }

        if (ptY == 1) {
            valueY = posY + TILE_SIZE;
        } else if (ptY == 2) {
            valueY = posY + valueA;
        } else if (ptY == 3) {
            valueY = posY + valueB;
        } else if (ptY == 4) {
            valueY = posY + valueC;
        } else if (ptY == 5) {
            valueY = posY + valueD;
        } else {
            valueY = posY;
        }

        vertexArray[vertexIndex + i].position = sf::Vector2f(valueX, valueY);
        vertexArray[vertexIndex + i].color = sf::Color(92 * average, 92 * average, 192 * average);
    }
}

