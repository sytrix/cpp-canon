#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "map_generator/MGWorld.hpp"
#include "map_generator/MGChunk.hpp"
#include "map_generator/MGLayerConfig.hpp"

#include "graphic/TileView.hpp"

#define CHUNK_SIZE 100

int main()
{
	srand(time(0));

	MGLayerConfig **layers = new MGLayerConfig*[3];
    layers[0] = new MGLayerConfig(5, 5, 1.0);
    layers[1] = new MGLayerConfig(20, 20, 0.4);
    //layers[2] = new MGLayerConfig(50, 50, 0.1);

    MGWorld *world = new MGWorld("sponge", layers, 2, CHUNK_SIZE);

	world->generateChunk(0, 0);

    MGChunk *chunk1 = world->getChunk(0, 0);

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(1600, 1200), "SFML window");
	window.setFramerateLimit(60);

	float *map = chunk1->getTerrainLevel();


	TileView ***mapTiles = new TileView**[CHUNK_SIZE];
	for (int x = 0; x < CHUNK_SIZE - 1; ++x) {
		mapTiles[x] = new TileView*[CHUNK_SIZE];
		for (int y = 0; y < CHUNK_SIZE - 1; ++y) {
			mapTiles[x][y] = new TileView(x, y, 
					map[x * CHUNK_SIZE + y], 
					map[(x + 1)  * CHUNK_SIZE + y], 
					map[(x + 1)  * CHUNK_SIZE + y + 1], 
					map[x * CHUNK_SIZE + y + 1], 
					0.75f);
		}
	}
	

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					
				} else if (event.key.code == sf::Keyboard::A) {
					
				}
			} else if (event.type == sf::Event::MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					std::cout << "click coordinate : " << event.mouseButton.x << "," << event.mouseButton.y << std::endl;
				}
			} else if (event.type == sf::Event::MouseMoved) {
				//gameManager.setMousePosition(sf::Vector2f(event.mouseMove.x, event.mouseMove.y));
			}
		}

		window.clear(sf::Color::Black);
		for (int x = 0; x < CHUNK_SIZE - 1; ++x) {
			for (int y = 0; y < CHUNK_SIZE - 1; ++y) {
				window.draw(*(mapTiles[x][y]));
			}
		}
		window.display();
	}

	for (int x = 0; x < CHUNK_SIZE - 1; ++x) {
		for (int y = 0; y < CHUNK_SIZE - 1; ++y) {
			delete mapTiles[x][y];
		}
		delete mapTiles[x];
	}
	delete mapTiles;

	

	return EXIT_SUCCESS;
}

