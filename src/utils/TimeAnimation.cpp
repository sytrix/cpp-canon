#include "TimeAnimation.hpp"

#include <cmath>
#include <iostream>

#if defined(WIN32) || defined(_WIN64)
#define _USE_MATH_DEFINES
#include "math.h"
#endif



float animDemiCos(float x) {
    return sin(x * M_PI_2);
}

float animCos(float x) {
    return sin(x * M_PI - M_PI_2) / 2.f + 0.5f;
}

float animZigZag(float x, float rebond) {
    return (1/(x*50+1)) * sin(x * M_PI * (1+rebond*2) - M_PI_2) + 1;
}


TimeAnimation::TimeAnimation()
: m_duration(0.f)
{
	
}

TimeAnimation::~TimeAnimation() 
{
	
}

void TimeAnimation::makeAnimation(float duration) 
{
    m_duration = duration;
    m_clock.restart();
}

bool TimeAnimation::isRunning()
{
    float clockTime = m_clock.getElapsedTime().asSeconds();
    if (clockTime < m_duration) {
        return true;
    }

    return false;
}

float TimeAnimation::getLinearInterpolation(float originalValue, float finalValue)
{
    float animationCoef = this->getAnimationCoef();
    return originalValue + (finalValue - originalValue) * animationCoef;
}

sf::Vector2f TimeAnimation::getLinearInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue)
{
    float animationCoef = this->getAnimationCoef();
    return sf::Vector2f(
        originalValue.x + (finalValue.x - originalValue.x) * animationCoef,
        originalValue.y + (finalValue.y - originalValue.y) * animationCoef
    );
}

float TimeAnimation::getDemiCosInterpolation(float originalValue, float finalValue)
{
    float coef = animDemiCos(this->getAnimationCoef());
    return originalValue + (finalValue - originalValue) * coef;
}

sf::Vector2f TimeAnimation::getDemiCosInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue)
{
    float coef = animDemiCos(this->getAnimationCoef());
    return sf::Vector2f(
        originalValue.x + (finalValue.x - originalValue.x) * coef,
        originalValue.y + (finalValue.y - originalValue.y) * coef
    );
}

float TimeAnimation::getCosInterpolation(float originalValue, float finalValue)
{
    float coef = animCos(this->getAnimationCoef());
    return originalValue + (finalValue - originalValue) * coef;
}

sf::Vector2f TimeAnimation::getCosInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue)
{
    float coef = animCos(this->getAnimationCoef());
    return sf::Vector2f(
        originalValue.x + (finalValue.x - originalValue.x) * coef,
        originalValue.y + (finalValue.y - originalValue.y) * coef
    );
}

float TimeAnimation::getZigZagInterpolation(float originalValue, float finalValue, float rebond)
{
    float coef = animZigZag(this->getAnimationCoef(), rebond);
    return originalValue + (finalValue - originalValue) * coef;
}

sf::Vector2f TimeAnimation::getZigZagInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue, float rebond)
{
    float coef = animZigZag(this->getAnimationCoef(), rebond);
    return sf::Vector2f(
        originalValue.x + (finalValue.x - originalValue.x) * coef,
        originalValue.y + (finalValue.y - originalValue.y) * coef
    );
}

float TimeAnimation::getAnimationCoef()
{
    float clockTime = m_clock.getElapsedTime().asSeconds();

    if (clockTime < m_duration) {
        return clockTime / m_duration;
    } else {
        return 1.f;
    }
}