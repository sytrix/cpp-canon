#ifndef TIMEANIMATION_H
#define TIMEANIMATION_H

#include <SFML/System/Clock.hpp>
#include <SFML/System/Vector2.hpp>

class TimeAnimation
{
	public:
		TimeAnimation();
		~TimeAnimation();

		void makeAnimation(float duration);
		bool isRunning();
		float getLinearInterpolation(float originalValue, float finalValue);
		sf::Vector2f getLinearInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue);
		float getDemiCosInterpolation(float originalValue, float finalValue);
		sf::Vector2f getDemiCosInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue);
		float getCosInterpolation(float originalValue, float finalValue);
		sf::Vector2f getCosInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue);
		float getZigZagInterpolation(float originalValue, float finalValue, float rebond);
		sf::Vector2f getZigZagInterpolation(sf::Vector2f originalValue, sf::Vector2f finalValue, float rebond);

		
		
	protected:
		
	private:

		float getAnimationCoef();

		sf::Clock m_clock;
		float m_duration;
};

#endif
