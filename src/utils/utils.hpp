#ifndef GAMESUTILS_H
#define GAMESUTILS_H

#include <SFML/System.hpp>

#if defined(WIN32) || defined(_WIN64)
#define isnanf std::isnan
#endif

sf::Vector3f equCart(sf::Vector2f posA, sf::Vector2f posB);
sf::Vector3<double> equCart(sf::Vector2<double> posA, sf::Vector2<double> posB);
sf::Vector3<double> perpendicularEquCartPassingThroughPoint(sf::Vector3<double> equCart, sf::Vector2<double> point);
sf::Vector2<double> pointDistanceMinDroite(sf::Vector3<double> cartA, sf::Vector2<double> point);
sf::Vector2<double> pointDistanceMinSegment(sf::Vector2<double> segmentPointA, sf::Vector2<double> segmentPointB, sf::Vector2<double> point);
bool isInsideTriangle(sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> pointToTest);
bool isInsideQuad(sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> d, sf::Vector2<double> pointToTest);
bool isInsidePenta(sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> d, sf::Vector2<double> e, sf::Vector2<double> pointToTest);
float distance(sf::Vector2i posA, sf::Vector2i posB);
float distance(sf::Vector2f posA, sf::Vector2f posB);
double distance(sf::Vector2<double> posA, sf::Vector2<double> posB);
sf::Vector2f intersectionPoint(sf::Vector3f cartA, sf::Vector3f cartB);
sf::Vector2<double> intersectionPoint(sf::Vector3<double> cartA, sf::Vector3<double> cartB);
bool checkPointBetween(sf::Vector2f pointToCheck, sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d);
bool checkPointBetween(sf::Vector2<double> pointToCheck, sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> d);

unsigned char getCode(float vUL, float vUR, float vDR, float vDL, float p);
float getValueAWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p);
float getValueBWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p);
float getValueCWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p);
float getValueDWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p);
sf::Vector2<double> getPointAWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p);
sf::Vector2<double> getPointBWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p);
sf::Vector2<double> getPointCWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p);
sf::Vector2<double> getPointDWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p);
int getNbPoint(unsigned char code);

const char vertexMatrix[16][18] = {
    
    {}, // 0
    {0, 0, 2, 0, 0, 3}, // 1
    {2, 0, 1, 0, 1, 3}, // 2
    {0, 0, 1, 0, 1, 2, 0, 0, 1, 2, 0, 3}, // 3
    {1, 2, 1, 1, 3, 1}, // 4
    {0, 0, 2, 0, 0, 3, 1, 5, 1, 1, 4, 1}, // 5 (1+4)
    {2, 0, 1, 0, 1, 1, 2, 0, 1, 1, 3, 1}, // 6
    {0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 2, 1, 0, 0, 2, 1, 0, 3}, // 7
    {0, 2, 3, 1, 0, 1}, // 8
    {0, 0, 2, 0, 3, 1, 0, 0, 3, 1, 0, 1}, // 9
    {2, 0, 1, 0, 1, 3, 0, 4, 5, 1, 0, 1}, // A (2+8)
    {0, 0, 1, 0, 1, 2, 0, 0, 1, 2, 3, 1, 0, 0, 3, 1, 0, 1}, // B
    {0, 2, 1, 3, 1, 1, 0, 2, 1, 1, 0, 1}, // C
    {0, 0, 2, 0, 1, 3, 0, 0, 1, 3, 1, 1, 0, 0, 1, 1, 0, 1}, // D
    {2, 0, 1, 0, 1, 1, 2, 0, 1, 1, 0, 1, 2, 0, 0, 1, 0, 3}, // E
    {0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1}, // F
    
};

#endif