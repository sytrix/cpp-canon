#include "utils.hpp"

#include <cmath>
#include <iostream>

sf::Vector3f equCart(sf::Vector2f posA, sf::Vector2f posB) 
{
    float vecx = posB.x - posA.x;
    float vecy = posB.y - posA.y;
    float a = vecy;
    float b = -vecx;
    float c = vecy * (-posA.x) - (vecx * (-posA.y));

    return sf::Vector3f(a, b, c);
}

sf::Vector3<double> equCart(sf::Vector2<double> posA, sf::Vector2<double> posB) 
{
    double vecx = posB.x - posA.x;
    double vecy = posB.y - posA.y;
    double a = vecy;
    double b = -vecx;
    double c = vecy * (-posA.x) - (vecx * (-posA.y));

    return sf::Vector3<double>(a, b, c);
}

sf::Vector3<double> perpendicularEquCartPassingThroughPoint(sf::Vector3<double> equCart, sf::Vector2<double> point)
{
    double a = equCart.y;
    double b = -equCart.x;
    double p = -(point.x * a + point.y * b);

    return sf::Vector3<double>(a, b, p);
}

sf::Vector2<double> pointDistanceMinDroite(sf::Vector3<double> cartA, sf::Vector2<double> point) {

    sf::Vector3<double> cartB = perpendicularEquCartPassingThroughPoint(cartA, point);

    double x;
    double y;

    if(cartA.x != 0) {
        double coef = -cartA.y / cartA.x;
        double dividende = cartB.y + coef * cartB.x;

        if(dividende != 0) {
            double term = -cartA.z / cartA.x;
            y = -(term * cartB.x + cartB.z) / dividende;
            x = coef * y + term; 
        } else {
            return sf::Vector2<double>(NAN, NAN);
        }
    } else {
        if(cartA.y != 0) {
            y = -cartA.z / cartA.y;

            if(cartB.x != 0) {
                x = -(cartB.y * y + cartB.z) / cartB.x;
            } else {
                return sf::Vector2<double>(NAN, NAN);
            }
        } else {
            return sf::Vector2<double>(NAN, NAN);
        }
    }

    return sf::Vector2<double>(x, y);
}

sf::Vector2<double> pointDistanceMinSegment(sf::Vector2<double> segmentPointA, sf::Vector2<double> segmentPointB, sf::Vector2<double> point) 
{
    sf::Vector3<double> cartA = equCart(segmentPointA, segmentPointB);
    sf::Vector2<double> pointMinDistance = pointDistanceMinDroite(cartA, point);

    if (!isnanf(pointMinDistance.x) && !isnanf(pointMinDistance.y)) {
        double length = distance(segmentPointA, segmentPointB);
        double distA = distance(segmentPointA, pointMinDistance);
        double distB = distance(segmentPointB, pointMinDistance);

        double pointDistA = distance(segmentPointA, point);
        double pointDistB = distance(segmentPointB, point);

        if(distA < length && distB < length) {
            return pointMinDistance;
        } else {
            if (pointDistA < pointDistB) {
                return segmentPointA;
            } else {
                return segmentPointB;
            }
        }
    }

    return sf::Vector2<double>(NAN, NAN);
}

double sign(sf::Vector2<double> p1, sf::Vector2<double> p2, sf::Vector2<double> p3)
{
    return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
}

bool isInsideTriangle(sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> pointToTest)
{
    double d1 = sign(pointToTest, a, b);
    double d2 = sign(pointToTest, b, c);
    double d3 = sign(pointToTest, c, a);

    //std::cout << "d:" << d1 << "," << d2 << "," << d3 << "," << std::endl;

    if (d1 >= 0.0 && d2 >= 0.0 && d3 >= 0.0) {
        return true;
    }

    return false;
}

bool isInsideQuad(sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> d, sf::Vector2<double> pointToTest)
{
    double d1 = sign(pointToTest, a, b);
    double d2 = sign(pointToTest, b, c);
    double d3 = sign(pointToTest, c, d);
    double d4 = sign(pointToTest, d, a);

    if (d1 >= 0.0 && d2 >= 0.0 && d3 >= 0.0 && d4 >= 0.0) {
        return true;
    }

    return false;
}

bool isInsidePenta(sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> d, sf::Vector2<double> e, sf::Vector2<double> pointToTest)
{
    double d1 = sign(pointToTest, a, b);
    double d2 = sign(pointToTest, b, c);
    double d3 = sign(pointToTest, c, d);
    double d4 = sign(pointToTest, d, e);
    double d5 = sign(pointToTest, e, a);

    if (d1 >= 0.0 && d2 >= 0.0 && d3 >= 0.0 && d4 >= 0.0 && d5 >= 0.0) {
        return true;
    }

    return false;
}

float distance(sf::Vector2i posA, sf::Vector2i posB) 
{
    float mx = posB.x - posA.x;
    float my = posB.y - posA.y;
    return sqrt(mx * mx + my * my);
}

float distance(sf::Vector2f posA, sf::Vector2f posB) 
{
    float mx = posB.x - posA.x;
    float my = posB.y - posA.y;
    return sqrt(mx * mx + my * my);
}

double distance(sf::Vector2<double> posA, sf::Vector2<double> posB) 
{
    double mx = posB.x - posA.x;
    double my = posB.y - posA.y;
    return sqrt(mx * mx + my * my);
}

sf::Vector2f intersectionPoint(sf::Vector3f cartA, sf::Vector3f cartB) 
{
    if (cartA.x == 0) {
        sf::Vector3f tmp = cartA;
        cartA = cartB;
        cartB = tmp;
    }

    float a = cartA.x + cartB.x;
    float b = cartA.y + cartB.y;
    float c = cartA.z + cartB.z;

    float x = 0;
    float y = 0;

    if(a != 0) {
        float op1b = -b / a;
        float op1c = -c / a;
        float op2b = cartA.y + cartA.x * op1b;
        
        if(op2b != 0) {
            float op2c = cartA.z + cartA.x * op1c;
            y = -op2c / op2b;
        } else {
            return sf::Vector2f(NAN, NAN);
        }
        float op4bc = cartA.y * y + cartA.z;
        x = -op4bc / cartA.x;

    } else {
        if(b != 0) {
            y = -c / b;
        } else {
            return sf::Vector2f(NAN, NAN);
        }

    }

    return sf::Vector2f(x, y);
}

sf::Vector2<double> intersectionPoint(sf::Vector3<double> cartA, sf::Vector3<double> cartB) 
{
    if (cartA.x == 0) {
        sf::Vector3<double> tmp = cartA;
        cartA = cartB;
        cartB = tmp;
    }

    double a = cartA.x + cartB.x;
    double b = cartA.y + cartB.y;
    double c = cartA.z + cartB.z;

    double x = 0;
    double y = 0;

    if(a != 0) {
        double op1b = -b / a;
        double op1c = -c / a;
        double op2b = cartA.y + cartA.x * op1b;
        
        if(op2b != 0) {
            double op2c = cartA.z + cartA.x * op1c;
            y = -op2c / op2b;
        } else {
            return sf::Vector2<double>(NAN, NAN);
        }
        double op4bc = cartA.y * y + cartA.z;
        x = -op4bc / cartA.x;

    } else {
        if(b != 0) {
            y = -c / b;
        } else {
            return sf::Vector2<double>(NAN, NAN);
        }

    }

    return sf::Vector2<double>(x, y);
}

bool checkPointBetween(sf::Vector2f pointToCheck, sf::Vector2f a, sf::Vector2f b, sf::Vector2f c, sf::Vector2f d)
{
    float length1 = distance(a, b);
    float dist1A = distance(a, pointToCheck);
    float dist1B = distance(b, pointToCheck);

    if(length1 >= dist1A && length1 >= dist1B) {
        float length2 = distance(c, d);
        float dist2A = distance(c, pointToCheck);
        float dist2B = distance(d, pointToCheck);

        if(length2 >= dist2A && length2 >= dist2B) {
            return true;
        }
    }

    return false;
}

bool checkPointBetween(sf::Vector2<double> pointToCheck, sf::Vector2<double> a, sf::Vector2<double> b, sf::Vector2<double> c, sf::Vector2<double> d)
{
    double length1 = distance(a, b);
    double dist1A = distance(a, pointToCheck);
    double dist1B = distance(b, pointToCheck);

    if(length1 > dist1A && length1 > dist1B) {
        double length2 = distance(c, d);
        double dist2A = distance(c, pointToCheck);
        double dist2B = distance(d, pointToCheck);

        if(length2 > dist2A && length2 > dist2B) {
            return true;
        }
    }

    return false;
}


unsigned char getCode(float vUL, float vUR, float vDR, float vDL, float p)
{
    unsigned char result = 0x00;
    if (vUL > p) {
        result |= 0x01;  // 0001
    }
    if (vUR > p) {
        result |= 0x02;  // 0010
    }
    if (vDR > p) {
        result |= 0x04;  // 0100
    }
    if (vDL > p) {
        result |= 0x08;  // 1000
    }
    
    return result;


}

float getValueAWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p)
{
    if (code == 0x0F) {return 0.f;}
    else if (code == 0x0E) {return 1.f - ((vUR-p) / (vUR-vUL));}
    else if (code == 0x0D) {return (vUL-p) / (vUL-vUR);}
    else if (code == 0x0C) {return 1.f - ((vDL-p) / (vDL-vUL));}
    else if (code == 0x0B) {return (vUR-p) / (vUR-vDR);}
    else if (code == 0x0A) {return 1.f - ((vUR-p) / (vUR-vUL));}
    else if (code == 0x09) {return (vUL-p) / (vUL-vUR);}
    else if (code == 0x08) {return 1.f - ((vDL-p) / (vDL-vUL));}
    else if (code == 0x07) {return 1.f - ((vDR-p) / (vDR-vDL));}
    else if (code == 0x06) {return 1.f - ((vUR-p) / (vUR-vUL));}
    else if (code == 0x05) {return (vUL-p) / (vUL-vUR);}
    else if (code == 0x04) {return 1.f - ((vDR-p) / (vDR-vUR));}
    else if (code == 0x03) {return (vUR-p) / (vUR-vDR);}
    else if (code == 0x02) {return 1.f - ((vUR-p) / (vUR-vUL));}
    else if (code == 0x01) {return (vUL-p) / (vUL-vUR);}
    
    return 0.f;
}

float getValueBWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p)
{
    if (code == 0x0F) {return 0.f;}
    else if (code == 0x0E) {return 1.f - ((vDL-p) / (vDL-vUL));}
    else if (code == 0x0D) {return 1.f - ((vDR-p) / (vDR-vUR));}
    else if (code == 0x0C) {return 1.f - ((vDR-p) / (vDR-vUR));}
    else if (code == 0x0B) {return (vDL-p) / (vDL-vDR);}
    else if (code == 0x0A) {return (vUR-p) / (vUR-vDR);}
    else if (code == 0x09) {return (vDL-p) / (vDL-vDR);}
    else if (code == 0x08) {return (vDL-p) / (vDL-vDR);}
    else if (code == 0x07) {return (vUL-p) / (vUL-vDL);}
    else if (code == 0x06) {return 1.f - ((vDR-p) / (vDR-vDL));}
    else if (code == 0x05) {return (vUL-p) / (vUL-vDL);}
    else if (code == 0x04) {return 1.f - ((vDR-p) / (vDR-vDL));}
    else if (code == 0x03) {return (vUL-p) / (vUL-vDL);}
    else if (code == 0x02) {return (vUR-p) / (vUR-vDR);}
    else if (code == 0x01) {return (vUL-p) / (vUL-vDL);}
    
    return 0.f;
}

float getValueCWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p)
{
    if (code == 0x0A) {return 1.f - ((vDL-p) / (vDL-vUL));}
    else if (code == 0x05) {return 1.f - ((vDR-p) / (vDR-vDL));}

    return 0.f;
}

float getValueDWithCode(unsigned char code, float vUL, float vUR, float vDR, float vDL, float p)
{
    if (code == 0x0A) {return (vDL-p) / (vDL-vDR);}
    else if (code == 0x05) {return 1.f - ((vDR-p) / (vDR-vUR));}

    return 0.f;
}

sf::Vector2<double> getPointAWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p) 
{
    if (code == 0x0E) {return sf::Vector2<double>(1.0 - ((vUR-p) / (vUR-vUL)), 0.0);}
    else if (code == 0x0D) {return sf::Vector2<double>((vUL-p) / (vUL-vUR), 0.0);}
    else if (code == 0x0C) {return sf::Vector2<double>(0.0, 1.0 - ((vDL-p) / (vDL-vUL)));}
    else if (code == 0x0B) {return sf::Vector2<double>(1.0, (vUR-p) / (vUR-vDR));}
    else if (code == 0x0A) {return sf::Vector2<double>(1.0 - ((vUR-p) / (vUR-vUL)), 0.0);}
    else if (code == 0x09) {return sf::Vector2<double>((vUL-p) / (vUL-vUR), 0.0);}
    else if (code == 0x08) {return sf::Vector2<double>(0.0, 1.0 - ((vDL-p) / (vDL-vUL)));}
    else if (code == 0x07) {return sf::Vector2<double>(1.0 - ((vDR-p) / (vDR-vDL)), 1.0);}
    else if (code == 0x06) {return sf::Vector2<double>(1.0 - ((vUR-p) / (vUR-vUL)), 0.0);}
    else if (code == 0x05) {return sf::Vector2<double>((vUL-p) / (vUL-vUR), 0.0);}
    else if (code == 0x04) {return sf::Vector2<double>(1.0, 1.0 - ((vDR-p) / (vDR-vUR)));}
    else if (code == 0x03) {return sf::Vector2<double>(1.0, (vUR-p) / (vUR-vDR));}
    else if (code == 0x02) {return sf::Vector2<double>(1.0 - ((vUR-p) / (vUR-vUL)), 0.0);}
    else if (code == 0x01) {return sf::Vector2<double>((vUL-p) / (vUL-vUR), 0.0);}
    
    return sf::Vector2<double>(0.0, 0.0);
}

sf::Vector2<double> getPointBWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p)
{
    if (code == 0x0E) {return sf::Vector2<double>(0.0, 1.0 - ((vDL-p) / (vDL-vUL)));}
    else if (code == 0x0D) {return sf::Vector2<double>(1.0, 1.0 - ((vDR-p) / (vDR-vUR)));}
    else if (code == 0x0C) {return sf::Vector2<double>(1.0, 1.0 - ((vDR-p) / (vDR-vUR)));}
    else if (code == 0x0B) {return sf::Vector2<double>((vDL-p) / (vDL-vDR), 1.0);}
    else if (code == 0x0A) {return sf::Vector2<double>(1.0, (vUR-p) / (vUR-vDR));}
    else if (code == 0x09) {return sf::Vector2<double>((vDL-p) / (vDL-vDR), 1.0);}
    else if (code == 0x08) {return sf::Vector2<double>((vDL-p) / (vDL-vDR), 1.0);}
    else if (code == 0x07) {return sf::Vector2<double>(0.0, (vUL-p) / (vUL-vDL));}
    else if (code == 0x06) {return sf::Vector2<double>(1.0 - ((vDR-p) / (vDR-vDL)), 1.0);}
    else if (code == 0x05) {return sf::Vector2<double>(0.0, (vUL-p) / (vUL-vDL));}
    else if (code == 0x04) {return sf::Vector2<double>(1.0 - ((vDR-p) / (vDR-vDL)), 1.0);}
    else if (code == 0x03) {return sf::Vector2<double>(0.0, (vUL-p) / (vUL-vDL));}
    else if (code == 0x02) {return sf::Vector2<double>(1.0, (vUR-p) / (vUR-vDR));}
    else if (code == 0x01) {return sf::Vector2<double>(0.0, (vUL-p) / (vUL-vDL));}
    
    return sf::Vector2<double>(0.0, 0.0);
}

sf::Vector2<double> getPointCWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p)
{
    if (code == 0x0A) {return sf::Vector2<double>(0.0, 1.0 - ((vDL-p) / (vDL-vUL)));}
    else if (code == 0x05) {return sf::Vector2<double>(1.0 - ((vDR-p) / (vDR-vDL)), 1.0);}
    
    return sf::Vector2<double>();
}

sf::Vector2<double> getPointDWithCode(unsigned char code, double vUL, double vUR, double vDR, double vDL, double p)
{
    if (code == 0x0A) {return sf::Vector2<double>((vDL-p) / (vDL-vDR), 1.0);}
    else if (code == 0x05) {return sf::Vector2<double>(1.0, 1.0 - ((vDR-p) / (vDR-vUR)));}
    
    return sf::Vector2<double>();
}



int getNbPoint(unsigned char code) {

    if (code == 0x0F) {return 6;}
    else if (code == 0x0E) {return 9;}
    else if (code == 0x0D) {return 9;}
    else if (code == 0x0C) {return 6;}
    else if (code == 0x0B) {return 9;}
    else if (code == 0x0A) {return 6;}
    else if (code == 0x09) {return 6;}
    else if (code == 0x08) {return 3;}
    else if (code == 0x07) {return 9;}
    else if (code == 0x06) {return 6;}
    else if (code == 0x05) {return 6;}
    else if (code == 0x04) {return 3;}
    else if (code == 0x03) {return 6;}
    else if (code == 0x02) {return 3;}
    else if (code == 0x01) {return 3;}
    
    return 0;
}

