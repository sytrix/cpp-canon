#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "graphic/WorldView.hpp"
#include "map_data/WorldData.hpp"

#include "const.hpp"
#include "utils/utils.hpp"

#define DETECTION_RADIUS 4.f

sf::Vector2f transformPoint(sf::View view, float pX, float pY) {
	//sf::Vector2f size = view.getSize() * 0.5f;
	sf::Vector2f size = sf::Vector2f(2000, 1200) * 0.5f;
	return view.getInverseTransform().transformPoint(sf::Vector2f((pX - size.x) / size.x, (size.y - pY) / size.y));
}

sf::Vector2f transformPoint(sf::View view, sf::Vector2f point) {
	return transformPoint(view, point.x, point.y);
}

int main()
{
	srand(time(0));

	// Create the main window
	unsigned int width = sf::VideoMode::getDesktopMode().width;
	unsigned int height = sf::VideoMode::getDesktopMode().height;
	sf::RenderWindow window(sf::VideoMode(width, height), "SFML window");
	window.setFramerateLimit(60);

#if defined(WIN32) || defined(_WIN64)
	window.setPosition(sf::Vector2i(-8, 0));
#endif

	float pValue = 0.75f;
	WorldData *worldData = new WorldData("bob", pValue);
	WorldView *worldView = new WorldView(worldData, pValue);

	sf::View view = window.getView();

	const float movementSpeed = 12.f;
	const float rotationSpeed = 0.2f;
	const float zoomSpeed = 0.02f;
	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;
	bool turnLeft = false;
	bool turnRight = false;
	bool zoomIn = false;
	bool zoomOut = false;

	bool mouseLeft = false;
	bool mouseRight = false;
	float mouseX = 0.f;
	float mouseY = 0.f;

	bool haveToRender = true;


	sf::Vector2f pointA;
	sf::Vector2f pointB;
	
	sf::VertexArray line(sf::PrimitiveType::Lines);
	line.append(sf::Vertex(pointA, sf::Color::Green));
	line.append(sf::Vertex(pointB, sf::Color::Green));
	sf::CircleShape circle(5.f, 12);
	circle.setOrigin(5.f, 5.f);
	circle.setFillColor(sf::Color(70, 110, 245));

	sf::CircleShape circleRadius(DETECTION_RADIUS * TILE_SIZE, 20);
	circleRadius.setOrigin(DETECTION_RADIUS * TILE_SIZE, DETECTION_RADIUS * TILE_SIZE);
	circleRadius.setFillColor(sf::Color(20, 128, 20, 50));

	sf::CircleShape insideCircle(5.f, 20);
	insideCircle.setOrigin(5.f, 5.f);
	insideCircle.setFillColor(sf::Color(20, 20, 20, 128));


	enum {MODE_COLLISION_TEST, MODE_MATERIAL_BRUSH, MODE_NEAREST_POINT, MODE_INSIDE_TEST};
	int mode = MODE_INSIDE_TEST;


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					
				} else if (event.key.code == sf::Keyboard::Escape) {
					window.close();
				}
				else if (event.key.code == sf::Keyboard::R) {
					pValue += 0.01f;
					if (pValue > 1.f) {
						pValue = 1.f;
					}
					//world.setPValue(pValue);
				} else if (event.key.code == sf::Keyboard::F) {
					pValue -= 0.01f;
					if (pValue < 0.f) {
						pValue = 0.f;
					}
					//world.setPValue(pValue);
				} else if (event.key.code == sf::Keyboard::Z) {
					up = true;
				} else if (event.key.code == sf::Keyboard::S) {
					down = true;
				} else if (event.key.code == sf::Keyboard::Q) {
					left = true;
				} else if (event.key.code == sf::Keyboard::D) {
					right = true;
				} else if (event.key.code == sf::Keyboard::A) {
					turnLeft = true;
				} else if (event.key.code == sf::Keyboard::E) {
					turnRight = true;
				} else if (event.key.code == sf::Keyboard::T) {
					zoomIn = true;
				} else if (event.key.code == sf::Keyboard::G) {
					zoomOut = true;
				} else if (event.key.code == sf::Keyboard::C) {
					mode = MODE_COLLISION_TEST;
				} else if (event.key.code == sf::Keyboard::V) {
					mode = MODE_MATERIAL_BRUSH;
				} else if (event.key.code == sf::Keyboard::X) {
					mode = MODE_NEAREST_POINT;
				}

			} else if (event.type == sf::Event::KeyReleased) {
				if (event.key.code == sf::Keyboard::Z) {
					up = false;
				} else if (event.key.code == sf::Keyboard::S) {
					down = false;
				} else if (event.key.code == sf::Keyboard::Q) {
					left = false;
				} else if (event.key.code == sf::Keyboard::D) {
					right = false;
				} else if (event.key.code == sf::Keyboard::A) {
					turnLeft = false;
				} else if (event.key.code == sf::Keyboard::E) {
					turnRight = false;
				} else if (event.key.code == sf::Keyboard::T) {
					zoomIn = false;
				} else if (event.key.code == sf::Keyboard::G) {
					zoomOut = false;
				}
			} else if (event.type == sf::Event::MouseButtonPressed) {
				mouseX = event.mouseButton.x;
				mouseY = event.mouseButton.y;

				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					mouseLeft = true;
					if (mode == MODE_COLLISION_TEST) {
						pointA = transformPoint(view, mouseX, mouseY);
						line[0].position = pointA;
					} else if (mode == MODE_MATERIAL_BRUSH) {
						worldData->brush(transformPoint(view, mouseX, mouseY), 6.f, 0.1f);
					}
					
					
				} else if (event.mouseButton.button == sf::Mouse::Button::Right) {
					mouseRight = true;
					if (mode == MODE_COLLISION_TEST) {
						pointB = transformPoint(view, mouseX, mouseY);
						line[1].position = pointB;
					} else if (mode == MODE_MATERIAL_BRUSH) {
						worldData->brush(transformPoint(view, mouseX, mouseY), 6.f, -0.1f);
					}
				}

				if (mode == MODE_COLLISION_TEST) {
					sf::Vector2f collisionPoint = worldData->getCollisionPoint(pointA, pointB);

					std::cout << "collision point : " << collisionPoint.x << "," << collisionPoint.y << std::endl;

					if (!isnanf(collisionPoint.x) && !isnanf(collisionPoint.y)) {
						circle.setPosition(collisionPoint);
					} else {
						circle.setPosition(sf::Vector2f(0, 0));
					}
				} else if (mode == MODE_NEAREST_POINT) {
					sf::Vector2f point = transformPoint(view, mouseX, mouseY);
					circleRadius.setPosition(point);
					sf::Vector2f nearestPoint = worldData->getNearestPoint(point, DETECTION_RADIUS);

					std::cout << "nearest point : " << nearestPoint.x << "," << nearestPoint.y << std::endl;

					if (!isnanf(nearestPoint.x) && !isnanf(nearestPoint.y)) {
						circle.setPosition(nearestPoint);
					} else {
						circle.setPosition(sf::Vector2f(0, 0));
					}
				}

			} else if (event.type == sf::Event::MouseButtonReleased) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					mouseLeft = false;
				} else if (event.mouseButton.button == sf::Mouse::Button::Right) {
					mouseRight = false;
				}
			} else if (event.type == sf::Event::MouseMoved) {
				mouseX = event.mouseMove.x;
				mouseY = event.mouseMove.y;
			}
		}

		if (up) {
			view.move(sf::Vector2f(0, -movementSpeed));
		}
		if (down) {
			view.move(sf::Vector2f(0, movementSpeed));
		}
		if (left) {
			view.move(sf::Vector2f(-movementSpeed, 0));
		}
		if (right) {
			view.move(sf::Vector2f(movementSpeed, 0));
		}
		if (turnLeft) {
			view.rotate(-rotationSpeed);
		}
		if (turnRight) {
			view.rotate(rotationSpeed);
		}
		if (zoomIn) {
			view.zoom(1.f - zoomSpeed);
		}
		if (zoomOut) {
			view.zoom(1.f + zoomSpeed);
		}

		if (mode == MODE_NEAREST_POINT) {
			sf::Vector2f point = transformPoint(view, mouseX, mouseY);
			circleRadius.setPosition(point);
			sf::Vector2f nearestPoint = worldData->getNearestPoint(point, DETECTION_RADIUS);

			if (!isnanf(nearestPoint.x) && !isnanf(nearestPoint.y)) {
				circle.setPosition(nearestPoint);
			} else {
				circle.setPosition(sf::Vector2f(0, 0));
			}
		} else if (mode == MODE_INSIDE_TEST) {
			
			sf::Vector2f point = transformPoint(view, mouseX, mouseY);
			insideCircle.setPosition(point);
			if(worldData->isInsideWall(point)) {
				insideCircle.setFillColor(sf::Color(255, 20, 20, 128));
			} else {
				insideCircle.setFillColor(sf::Color(20, 255, 20, 128));
			}
		} else if (mode == MODE_MATERIAL_BRUSH) {
			sf::Vector2f point = transformPoint(view, mouseX, mouseY);
			if (mouseLeft) {
				worldData->brush(point, 6.f, 0.01f);
			}
			if (mouseRight) {
				worldData->brush(point, 6.f, -0.01f);
			}
		}

		if (up || down || left || right || turnLeft || turnRight || zoomIn || zoomOut || haveToRender) {
			worldView->setView(view);
			window.setView(view);
		}

		worldView->updateRender();

		
		//if (mouseLeft || mouseRight || up || down || left || right || turnLeft || turnRight || zoomIn || zoomOut || haveToRender) {
		window.clear(sf::Color::Black);
		window.draw(*worldView);
		window.draw(line);
		window.draw(circle);
		if (mode == MODE_NEAREST_POINT) {
			window.draw(circleRadius);
		} else if (mode == MODE_INSIDE_TEST) {
			window.draw(insideCircle);
		}
		haveToRender = false;
		//}

		window.display();
	}

	delete worldView;
	delete worldData;

	return EXIT_SUCCESS;
}

