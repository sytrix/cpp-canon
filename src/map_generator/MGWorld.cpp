#include "MGWorld.hpp"

#include "MGChunk.hpp"



MGWorld::MGWorld(std::string seedStr, MGLayerConfig **layers, int nbLayer, int chunkSize) 
: m_seedStr(seedStr), m_layers(layers), m_nbLayer(nbLayer), m_chunkSize(chunkSize)
{

}

MGWorld::~MGWorld() {
	
    std::map<std::string, MGChunk*>::iterator itr; 
    for (itr = m_chunks.begin(); itr != m_chunks.end(); ++itr) { 
        delete itr->second;
    }
    delete m_layers;
}

bool MGWorld::generateChunk(int x, int y) 
{
    std::string chunkKey = this->chunkKey(x, y);
    std::map<std::string, MGChunk *>::iterator it = m_chunks.find(chunkKey);

    if (it != m_chunks.end()) {
        MGChunk *chunk = it->second;
        if (chunk->isComplete()) {
            return false;
        } else {
            chunk->generate(m_layers, m_nbLayer, m_chunkSize);
            return true;
        }
    } else {
        MGChunk *chunk = new MGChunk(this, m_seedStr + ";" + chunkKey, x, y, m_layers, m_nbLayer);
        chunk->generate(m_layers, m_nbLayer, m_chunkSize);
        m_chunks[chunkKey] = chunk;
        return true;
    }
}

MGChunk *MGWorld::preGenerateChunk(int x, int y) 
{
    std::string chunkKey = this->chunkKey(x, y);
    std::map<std::string, MGChunk *>::iterator it = m_chunks.find(chunkKey);

    if (it != m_chunks.end()) {
        return it->second;
    } else {
        MGChunk *chunk = new MGChunk(this, m_seedStr + ";" + chunkKey, x, y, m_layers, m_nbLayer);
        m_chunks[chunkKey] = chunk;
        return chunk;
    }
}

MGChunk *MGWorld::getChunk(int chunkX, int chunkY)
{
    std::string chunkKey = this->chunkKey(chunkX, chunkY);
    std::map<std::string, MGChunk *>::iterator it = m_chunks.find(chunkKey);

    if (it != m_chunks.end()) {
        return it->second;
    }

    return nullptr;
}

std::string MGWorld::chunkKey(int x, int y)
{
    return std::to_string(x) + ";" + std::to_string(y);
}
