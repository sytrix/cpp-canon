#ifndef WORLD_H
#define WORLD_H

#include <map>
#include <string>
class MGChunk;
class MGLayerConfig;

class MGWorld
{
	public:
		MGWorld(std::string seedStr, MGLayerConfig **layers, int nbLayer, int chunkSize);
		~MGWorld();
		
		bool generateChunk(int x, int y);
		MGChunk *preGenerateChunk(int x, int y);
		MGChunk *getChunk(int chunkX, int chunkY);

	protected:
		
	private:

		std::string chunkKey(int x, int y);

		std::map<std::string, MGChunk*> m_chunks;
		std::string m_seedStr;
		MGLayerConfig **m_layers;
		int m_nbLayer;
		int m_chunkSize;
		
};

#endif
