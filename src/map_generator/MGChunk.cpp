#include "MGChunk.hpp"

#include <iostream>
#include <string.h>

#include "MGLayerConfig.hpp"
#include "MGWorld.hpp"

#include <boost/random.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/container_hash/hash.hpp>

#define AMPLITUDE_PRECISION         10000


inline 
float interpolation(float A, float B, float C, float D, float t, float t2, float t3)
{
    float a = -A / 2.0f + (3.0f*B) / 2.0f - (3.0f*C) / 2.0f + D / 2.0f;
    float b = A - (5.0f*B) / 2.0f + 2.0f*C - D / 2.0f;
    float c = -A / 2.0f + C / 2.0f;
    float d = B;
 
    return a*t3 + b*t2 + c*t + d;
}


MGChunk::MGChunk(MGWorld *world, std::string seedStr, int chunkX, int chunkY, MGLayerConfig **layers, int layerCount)
: m_world(world), m_chunkX(chunkX), m_chunkY(chunkY), m_layerCount(layerCount), m_terrainLevel(nullptr), m_terrainBiome(nullptr)
{
	boost::hash<std::string> hashFn;
	std::size_t seed = hashFn(seedStr);

    m_layersData = new float**[m_layerCount];
    for (int layerIndex = 0; layerIndex < m_layerCount; layerIndex++) {

        float **perlinNoiseMaps = new float*[NB_PERLIN_NOISE_MAP];
        for (int mapId = 0; mapId < NB_PERLIN_NOISE_MAP; mapId++) {

            perlinNoiseMaps[mapId] = this->createLayer(seed + layerIndex * NB_PERLIN_NOISE_MAP + mapId, layers[layerIndex]);
            
        }
        m_layersData[layerIndex] = perlinNoiseMaps;
    }
}

MGChunk::~MGChunk() {
	
    for (int i = 0; i < m_layerCount; ++i) {
        for (int mapId = 0; mapId < NB_PERLIN_NOISE_MAP; ++mapId) {
            delete m_layersData[i][mapId];
        }
        delete m_layersData[i];
    }
    delete m_layersData;
    delete m_terrainLevel;
    delete m_terrainBiome;
}

float *MGChunk::createLayer(size_t seed, MGLayerConfig *layer) {
	boost::minstd_rand generator(seed);
	boost::uniform_real<> uni_dist(0, 1);
	boost::variate_generator<boost::minstd_rand&, boost::uniform_real<> > uni(generator, uni_dist);
    int layerWidth = layer->getWidth();
    int layerHeight = layer->getHeight();
    int layerSize = layerWidth * layerHeight;

    float *layerSurface = new float[layerSize];

    for (int i = 0; i < layerSize; i++) {
		layerSurface[i] = uni();
    }

    return layerSurface;
}

bool MGChunk::isComplete()
{
    return m_terrainLevel != nullptr;
}

void MGChunk::generate(MGLayerConfig **layers, int nbLayer, int chunkSize)
{
    
    MGChunk *chunkList[9];

    for (int y = 0; y < 3; ++y) {
        for (int x = 0; x < 3; ++x) {
            chunkList[y*3+x] = m_world->preGenerateChunk(m_chunkX + x - 1, m_chunkY + y - 1);
        }
    }

    float **terrains = new float*[NB_PERLIN_NOISE_MAP];

    for (int mapId = 0; mapId < NB_PERLIN_NOISE_MAP; ++mapId) {

        float *terrain = new float[chunkSize * chunkSize];
        //bzero(terrain, chunkSize * chunkSize * sizeof(float));
		memset(terrain, 0, chunkSize * chunkSize * sizeof(float));

        for (int layerIndex = 0; layerIndex < m_layerCount; ++layerIndex) {
            this->interpolationLayer(layers[layerIndex], m_layersData[layerIndex][mapId], terrain, chunkSize);
        }
        
        for (int layerIndex = 0; layerIndex < m_layerCount; ++layerIndex) {
            float *listOfLayersData[9];

            for (int chunkId = 0; chunkId < 9; ++chunkId) {
                listOfLayersData[chunkId] = chunkList[chunkId]->m_layersData[layerIndex][mapId];
            }
            
            this->interpolationLayerBorder(layers[layerIndex], listOfLayersData, terrain, chunkSize);
        }

        terrains[mapId] = terrain;
    }

    m_terrainLevel = terrains[PERLIN_NOISE_LEVEL];

    // TODO : make perlin noise combination for biome generation
    m_terrainBiome = new char[chunkSize * chunkSize];
    //bzero(m_terrainBiome, chunkSize * chunkSize);
    this->combination(terrains, m_terrainBiome, PERLIN_NOISE_LOCAL_BIOME_A, PERLIN_NOISE_LOCAL_BIOME_F, chunkSize);
    
    for (int mapId = PERLIN_NOISE_LOCAL_BIOME_A; mapId <= PERLIN_NOISE_LOCAL_BIOME_F; ++mapId) {
        delete terrains[mapId];
    }

    delete[] terrains;
}

void MGChunk::combination(float **terrains, char *result, int startId, int endId, int chunkSize) 
{

    for (int pixelId = 0; pixelId < chunkSize * chunkSize; ++pixelId) {
        float maxPixelValue = terrains[startId][pixelId];
        char maxMapId = startId;
        for (int mapId = startId + 1; mapId <= endId; ++mapId) {
            int pixelValue = terrains[mapId][pixelId];
            if (pixelValue > maxPixelValue) {
                maxPixelValue = pixelValue;
                maxMapId = mapId;
            }
        }

        result[pixelId] = maxMapId - startId;
    }
    
}

float MGChunk::amplitudeOfChunks(float **amplitudesAtLayer, int layerWidth, int coordX, int coordY) {

    if (coordY < 0) {
        if (coordX < 0) {
            return amplitudesAtLayer[0][(layerWidth * layerWidth) - 1];
        } else if(coordX < layerWidth) {
            return amplitudesAtLayer[1][(layerWidth - 1) * layerWidth + coordX];
        } else {
            return amplitudesAtLayer[2][(layerWidth - 1) * layerWidth + coordX - layerWidth];
        }
    } else if (coordY < layerWidth) {
        if (coordX < 0) {
            return amplitudesAtLayer[3][(layerWidth * (coordY + 1)) - 1];
        } else if(coordX < layerWidth) {
            return amplitudesAtLayer[4][(layerWidth * coordY) + coordX];
        } else {
            return amplitudesAtLayer[5][(layerWidth * coordY) + coordX - layerWidth];
        }
    } else {
        if (coordX < 0) {
            return amplitudesAtLayer[6][(layerWidth * (coordY - layerWidth + 1)) - 1];
        } else if(coordX < layerWidth) {
            return amplitudesAtLayer[7][(layerWidth * (coordY - layerWidth)) + coordX];
        } else {
            return amplitudesAtLayer[8][(layerWidth * (coordY - layerWidth)) + coordX - layerWidth];
        }
    }
    

    return 1.f;
}

void MGChunk::interpolationLayerBorder(MGLayerConfig *layer, float **amplitudesAtLayer, float *result, int chunkSize) {
    
    int layerWidth = layer->getWidth();
    int layerHeight = layer->getHeight();
    float amplitude = layer->getAmplitude();

    float deltaX = (float)layerWidth / (float)chunkSize;
    float deltaY = (float)layerHeight / (float)chunkSize;
    int mulX = chunkSize / layerWidth;
    int mulY = chunkSize / layerHeight;

    for (int x = 0; x < layerWidth; x++) {
        for (int y = 0; y < layerHeight; y++) {
            if (x >= 1 && y >= 1 && x < layerWidth - 2 && y < layerHeight - 2) {
                y = layerHeight - 2;
            } 

            float p00 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x - 1, y - 1);
            float p01 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x,     y - 1);
            float p02 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 1, y - 1);
            float p03 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 2, y - 1);

            float p10 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x - 1, y);
            float p11 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x,     y);
            float p12 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 1, y);
            float p13 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 2, y);

            float p20 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x - 1, y + 1);
            float p21 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x,     y + 1);
            float p22 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 1, y + 1);
            float p23 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 2, y + 1);

            float p30 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x - 1, y + 2);
            float p31 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x,     y + 2);
            float p32 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 1, y + 2);
            float p33 = this->amplitudeOfChunks(amplitudesAtLayer, layerWidth, x + 2, y + 2);
            
            float xfract = 0.f;
            for (int fx = 0; fx < mulX; ++fx) {
                
                
                float xfract2 = xfract*xfract;
                float xfract3 = xfract2*xfract;
                float col0 = interpolation(p00, p01, p02, p03, xfract, xfract2, xfract3);
                float col1 = interpolation(p10, p11, p12, p13, xfract, xfract2, xfract3);
                float col2 = interpolation(p20, p21, p22, p23, xfract, xfract2, xfract3);
                float col3 = interpolation(p30, p31, p32, p33, xfract, xfract2, xfract3);

                float yfract = 0.f;
                for (int fy = 0; fy < mulY; ++fy) {
                    
                    float yfract2 = yfract*yfract;
                    float yfract3 = yfract2*yfract;

                    float value = interpolation(col0, col1, col2, col3, yfract, yfract2, yfract3);
                    if ((x * mulX + fx) < chunkSize && (y * mulY + fy) < chunkSize) {
                        result[x * mulX + fx + (y * mulY + fy) * chunkSize] += value * amplitude;
                    }

                    yfract += deltaY;
                }

                xfract += deltaX;
            }
        }
    }
}

void MGChunk::interpolationLayer(MGLayerConfig *layer, float *amplitudeMap, float *result, int chunkSize) {
    
    int layerWidth = layer->getWidth();
    int layerHeight = layer->getHeight();
    float amplitude = layer->getAmplitude();

    float deltaX = (float)layerWidth / (float)chunkSize;
    float deltaY = (float)layerHeight / (float)chunkSize;
    int mulX = chunkSize / layerWidth;
    int mulY = chunkSize / layerHeight;

    int layerWidth2 = layerWidth * 2;
    for (int x = 1; x < layerWidth - 2; x++) {
        for (int y = 1; y < layerHeight - 2; y++) {
            int index = x + y * layerWidth;

            int indexM1 = index - 1;
            int indexP1 = index + 1;
            int indexP2 = index + 2;
            
            float p00 = amplitudeMap[indexM1 - layerWidth];
            float p01 = amplitudeMap[index - layerWidth];
            float p02 = amplitudeMap[indexP1 - layerWidth];
            float p03 = amplitudeMap[indexP2 - layerWidth];

            float p10 = amplitudeMap[indexM1];
            float p11 = amplitudeMap[index];
            float p12 = amplitudeMap[indexP1];
            float p13 = amplitudeMap[indexP2];

            float p20 = amplitudeMap[indexM1 + layerWidth];
            float p21 = amplitudeMap[index + layerWidth];
            float p22 = amplitudeMap[indexP1 + layerWidth];
            float p23 = amplitudeMap[indexP2 + layerWidth];

            float p30 = amplitudeMap[indexM1 + layerWidth2];
            float p31 = amplitudeMap[index + layerWidth2];
            float p32 = amplitudeMap[indexP1 + layerWidth2];
            float p33 = amplitudeMap[indexP2 + layerWidth2];

            float xfract = 0.f;
            for (int fx = 0; fx < mulX; ++fx) {
                
                
                float xfract2 = xfract*xfract;
                float xfract3 = xfract2*xfract;
                float col0 = interpolation(p00, p01, p02, p03, xfract, xfract2, xfract3);
                float col1 = interpolation(p10, p11, p12, p13, xfract, xfract2, xfract3);
                float col2 = interpolation(p20, p21, p22, p23, xfract, xfract2, xfract3);
                float col3 = interpolation(p30, p31, p32, p33, xfract, xfract2, xfract3);

                float yfract = 0.f;
                for (int fy = 0; fy < mulY; ++fy) {
                    
                    float yfract2 = yfract*yfract;
                    float yfract3 = yfract2*yfract;

                    float value = interpolation(col0, col1, col2, col3, yfract, yfract2, yfract3);
                    result[x * mulX + fx + (y * mulY + fy) * chunkSize] += value * amplitude;

                    yfract += deltaY;
                }

                xfract += deltaX;
            }
        }
    }
}

float *MGChunk::getTerrainLevel()
{
    return m_terrainLevel;
}


char *MGChunk::getTerrainBiome()
{
    return m_terrainBiome;
}