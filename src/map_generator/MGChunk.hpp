#ifndef CHUNK_H
#define CHUNK_H

#include <string>
class MGWorld;
class MGLayerConfig;

class MGChunk
{
	public:
		MGChunk(MGWorld *world, std::string seedStr, int chunkX, int chunkY, MGLayerConfig **layers, int layerCount);
		~MGChunk();

		enum {
			PERLIN_NOISE_LEVEL,
			PERLIN_NOISE_LOCAL_BIOME_A,
			PERLIN_NOISE_LOCAL_BIOME_B,
			PERLIN_NOISE_LOCAL_BIOME_C,
			PERLIN_NOISE_LOCAL_BIOME_D,
			PERLIN_NOISE_LOCAL_BIOME_E,
			PERLIN_NOISE_LOCAL_BIOME_F,


			NB_PERLIN_NOISE_MAP
		};
		
		bool isComplete();
		void generate(MGLayerConfig **layers, int nbLayer, int chunkSize);
		float *getTerrainLevel();
		char *getTerrainBiome();

	protected:

		float *createLayer(size_t seed, MGLayerConfig *layer);
		void combination(float **terrains, char *result, int startId, int endId, int chunkSize) ;
		float amplitudeOfChunks(float **amplitudesAtLayer, int layerWidth, int coordX, int coordY);
		void interpolationLayerBorder(MGLayerConfig *layer, float **amplitudesAtLayer, float *result, int chunkSize);
		void interpolationLayer(MGLayerConfig *layer, float *amplitudeMap, float *result, int chunkSize);
		
	private:

		

		MGWorld *m_world;
		int m_chunkX;
		int m_chunkY;

		float ***m_layersData;
		int m_layerCount;

		float *m_terrainLevel;
		char *m_terrainBiome;
		
};

#endif
