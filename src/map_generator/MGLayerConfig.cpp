#include "MGLayerConfig.hpp"



MGLayerConfig::MGLayerConfig(int width, int height, float amplitude) 
: m_width(width), m_height(height), m_amplitude(amplitude)
{
	
}

MGLayerConfig::~MGLayerConfig() 
{
	
}

int MGLayerConfig::getWidth()
{
    return m_width;
}

int MGLayerConfig::getHeight()
{
    return m_height;
}

float MGLayerConfig::getAmplitude()
{
    return m_amplitude;
}

