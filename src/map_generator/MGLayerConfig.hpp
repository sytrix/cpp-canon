#ifndef LAYERCONFIG_H
#define LAYERCONFIG_H

class MGLayerConfig
{
	public:
		MGLayerConfig(int width, int height, float amplitude);
		~MGLayerConfig();

		int getWidth();
		int getHeight();
		float getAmplitude();
		
	protected:
		
	private:
		int m_width;
		int m_height;
		float m_amplitude;
		
};

#endif
