#ifndef CHUNK_DATA_H
#define CHUNK_DATA_H

#include <SFML/System/Vector2.hpp>
class WorldData;

class ChunkData
{
	public:
		ChunkData(WorldData *worldData, sf::Vector2i chunkPosition);
		~ChunkData();

		float *getData();
		bool isModified();
		bool isRender();
		void render();

		void setTerrain(float *tUL, float *tUR, float *tDR, float *tDL);
		void addPointValue(sf::Vector2i tilePosition, float addValue);

		sf::Vector2<double> getCollisionPoint(sf::Vector2i tilePos, sf::Vector2<double> pointA, sf::Vector2<double> pointB, double pValue);
		sf::Vector2<double> getNearestPoint(sf::Vector2i tilePos, sf::Vector2<double> relativePoint, double pValue);
		bool isInside(sf::Vector2i tilePos, sf::Vector2<double> relativePoint, double pValue);
		
	protected:
		
	private:
		
		WorldData *m_worldData;
		sf::Vector2i m_chunkPosition;
		float *m_data;

		bool m_isModified;
		bool m_isRender;
};

#endif
