#include "WorldData.hpp"

#include <iostream>
#include <cmath>
#include <cfloat>

#include "../map_generator/MGWorld.hpp"
#include "../map_generator/MGLayerConfig.hpp"
#include "../map_generator/MGChunk.hpp"

#include "ChunkData.hpp"

#include "../const.hpp"
#include "../utils/utils.hpp"

WorldData::WorldData(std::string seed, float pValue) 
:
    m_pValue(pValue)
{
	MGLayerConfig **layers = new MGLayerConfig*[3];
    layers[0] = new MGLayerConfig(5, 5, 1.0);
    layers[1] = new MGLayerConfig(20, 20, 0.4);
    layers[2] = new MGLayerConfig(100, 100, 0.05);

    m_worldGenerator = new MGWorld(seed, layers, 3, CHUNK_SIZE);
}

WorldData::~WorldData() 
{
	delete m_worldGenerator;
}

void WorldData::brush(sf::Vector2f position, float radius, float addValue)
{
    sf::Vector2i start(floorf(position.x / TILE_SIZE - radius), floorf(position.y / TILE_SIZE - radius));
    sf::Vector2i end(ceilf(position.x / TILE_SIZE + radius), ceilf(position.y / TILE_SIZE + radius));

    for (int x = start.x; x <= end.x; ++x) {
        for (int y = start.y; y <= end.y; ++y) {
            sf::Vector2i tilePos(x, y);
            float dist = distance(tilePos, sf::Vector2i(position.x / TILE_SIZE, position.y / TILE_SIZE));
            if (dist < radius) {
                float value = cos(dist * M_PI_2 / radius);
                this->addPointValue(tilePos, value * addValue);
            }
        }
    }
}

sf::Vector2f WorldData::getCollisionPoint(sf::Vector2f pointA, sf::Vector2f pointB)
{
    sf::Vector2<double> doublePointA = sf::Vector2<double>(pointA.x / TILE_SIZE, pointA.y / TILE_SIZE);
    sf::Vector2<double> doublePointB = sf::Vector2<double>(pointB.x / TILE_SIZE, pointB.y / TILE_SIZE);

    float minX = std::min(pointA.x, pointB.x);
    float minY = std::min(pointA.y, pointB.y);
    float maxX = std::max(pointA.x, pointB.x);
    float maxY = std::max(pointA.y, pointB.y);

    int startX = ceil(minX / TILE_SIZE);
    int startY = ceil(minY / TILE_SIZE);
    int endX = floor(maxX / TILE_SIZE);
    int endY = floor(maxY / TILE_SIZE);

    //std::cout << "size:" << std::to_string(endX - startX) << "," << std::to_string(endY - startY) << std::endl;

    sf::Vector3<double> equationCart = equCart(
            sf::Vector2<double>(pointA.x / TILE_SIZE, pointA.y / TILE_SIZE), 
            sf::Vector2<double>(pointB.x / TILE_SIZE, pointB.y / TILE_SIZE)
        );

    //std::cout << "a:" << equationCart.x << " b:" << equationCart.y << " c:" << equationCart.z << std::endl;
    std::map<std::string, sf::Vector2i> intersectionTiles;

    intersectionTiles[this->chunkKey(endX, endY)] = sf::Vector2i(endX, endY);

    if (equationCart.y != 0.f) {
        for (double x = startX; x <= endX; ++x) {
            double y = -equationCart.x * x / equationCart.y - equationCart.z / equationCart.y;
            //std::cout << "x:" << x << " -> y:" << y << std::endl;
            double yIntersection = floor(y);
            int ox = x;
            int oy = yIntersection;
            intersectionTiles[this->chunkKey(ox-1, oy)] = sf::Vector2i(ox-1, oy);
            intersectionTiles[this->chunkKey(ox, oy)] = sf::Vector2i(ox, oy);
        }
    }
    if (equationCart.x != 0.f) {
        for (double y = startY; y <= endY; ++y) {
            double x = -equationCart.y * y / equationCart.x - equationCart.z / equationCart.x;
            //std::cout << "y:" << y << " -> x:" << x << std::endl;
            double xIntersection = floorf(x);
            int ox = xIntersection;
            int oy = y;
            intersectionTiles[this->chunkKey(ox, oy-1)] = sf::Vector2i(ox, oy-1);
            intersectionTiles[this->chunkKey(ox, oy)] = sf::Vector2i(ox, oy);
        }
    }

    //std::cout << "nb intersection:" << intersectionTiles.size() << std::endl;
    //for (std::map<std::string, sf::Vector2i>::iterator itr = intersectionTiles.begin(); itr != intersectionTiles.end(); ++itr) {
    //     std::cout << "coordX:" << itr->first << std::endl;
    //}

    double minDistance = DBL_MAX;
    sf::Vector2<double> currentCollisionPoint(NAN, NAN);

    for (std::map<std::string, sf::Vector2i>::iterator itr = intersectionTiles.begin(); itr != intersectionTiles.end(); ++itr) {
        sf::Vector2i pos = itr->second;
        int x = pos.x;
        int y = pos.y;

        int chunkX;
        int chunkY;
        int tileX;
        int tileY;
        
        if (x >= 0) {
            chunkX = x / CHUNK_SIZE;
            tileX = x % CHUNK_SIZE;
        } else {
            chunkX = (x + 1) / CHUNK_SIZE - 1;
            tileX = CHUNK_SIZE + ((x + 1) % CHUNK_SIZE) - 1;
        }
        if (y >= 0) {
            chunkY = y / CHUNK_SIZE;
            tileY = y % CHUNK_SIZE;
        } else {
            chunkY = y / CHUNK_SIZE - 1;
            tileY = CHUNK_SIZE + ((y + 1) % CHUNK_SIZE) - 1;
        }

        sf::Vector2<double> realPos(
            (chunkX * CHUNK_SIZE + tileX),
            (chunkY * CHUNK_SIZE + tileY)
        );

        ChunkData *chunkData = this->getChunkOrGenerateIt(chunkX, chunkY);
        sf::Vector2<double> collisionPoint = chunkData->getCollisionPoint(sf::Vector2i(tileX, tileY), doublePointA - realPos, doublePointB - realPos, m_pValue) + realPos;

        if (!isnanf(collisionPoint.x) && !isnanf(collisionPoint.y)) {
            double currentDistance = distance(doublePointA, collisionPoint);
            if (minDistance > currentDistance) {
                minDistance = currentDistance;
                currentCollisionPoint = collisionPoint;
            }
        }
    }

    return sf::Vector2f(currentCollisionPoint.x * TILE_SIZE, currentCollisionPoint.y * TILE_SIZE);
}

sf::Vector2f WorldData::getNearestPoint(sf::Vector2f point, float radius)
{
    sf::Vector2<double> doublePoint = sf::Vector2<double>(point.x / TILE_SIZE, point.y / TILE_SIZE);

    int minX = floor(doublePoint.x - radius);
    int minY = floor(doublePoint.y - radius);
    int maxX = ceil(doublePoint.x + radius);
    int maxY = ceil(doublePoint.y + radius);

    //std::cout << "size : " << std::to_string(maxX - minX + 1) << "," << std::to_string(maxY - minY + 1) << std::endl;


    sf::Vector2<double> currentNearestPoint = sf::Vector2<double>(NAN, NAN);
    double minDistanceToNearestPoint = DBL_MAX;

    for (int x = minX; x <= maxX; ++x) {
        for (int y = minY; y <= maxY; ++y) {
            int chunkX;
            int chunkY;
            int tileX;
            int tileY;
            
            if (x >= 0) {
                chunkX = x / CHUNK_SIZE;
                tileX = x % CHUNK_SIZE;
            } else {
                chunkX = (x + 1) / CHUNK_SIZE - 1;
                tileX = CHUNK_SIZE + ((x + 1) % CHUNK_SIZE) - 1;
            }
            if (y >= 0) {
                chunkY = y / CHUNK_SIZE;
                tileY = y % CHUNK_SIZE;
            } else {
                chunkY = y / CHUNK_SIZE - 1;
                tileY = CHUNK_SIZE + ((y + 1) % CHUNK_SIZE) - 1;
            }

            sf::Vector2<double> relativePoint(
                chunkX * CHUNK_SIZE + tileX,
                chunkY * CHUNK_SIZE + tileY
            );

            ChunkData *chunkData = this->getChunkOrGenerateIt(chunkX, chunkY);
            sf::Vector2<double> nearestPoint = chunkData->getNearestPoint(sf::Vector2i(tileX, tileY), doublePoint - relativePoint, m_pValue) + relativePoint;

            
            if (!isnanf(nearestPoint.x) && !isnanf(nearestPoint.y)) {
                double currentDistance = distance(doublePoint, nearestPoint);
                if (minDistanceToNearestPoint > currentDistance) {
                    minDistanceToNearestPoint = currentDistance;
                    currentNearestPoint = nearestPoint;
                }
            }
        }
    }

    if (minDistanceToNearestPoint > radius) {
        return sf::Vector2f(NAN, NAN);
    }
    return sf::Vector2f(currentNearestPoint.x * TILE_SIZE, currentNearestPoint.y * TILE_SIZE);
}

bool WorldData::isInsideWall(sf::Vector2f point) {

    sf::Vector2<double> doublePoint = sf::Vector2<double>(point.x, point.y);

    int chunkX;
    int chunkY;
    int tileX;
    int tileY;

    int x = floor(doublePoint.x / TILE_SIZE);
    int y = floor(doublePoint.y / TILE_SIZE);
    
    if (x >= 0) {
        chunkX = x / CHUNK_SIZE;
        tileX = x % CHUNK_SIZE;
    } else {
        chunkX = (x + 1) / CHUNK_SIZE - 1;
        tileX = CHUNK_SIZE + ((x + 1) % CHUNK_SIZE) - 1;
    }
    if (y >= 0) {
        chunkY = y / CHUNK_SIZE;
        tileY = y % CHUNK_SIZE;
    } else {
        chunkY = y / CHUNK_SIZE - 1;
        tileY = CHUNK_SIZE + ((y + 1) % CHUNK_SIZE) - 1;
    }

    //std::cout << "tile:" << std::to_string(tileX) << "," << std::to_string(tileY) << std::endl;
    //std::cout << "chunk:" << std::to_string(chunkX) << "," << std::to_string(chunkY) << std::endl;

    sf::Vector2<double> realPos(
        chunkX * CHUNK_SIZE + tileX,
        chunkY * CHUNK_SIZE + tileY
    );

    sf::Vector2<double> relativePoint(doublePoint.x / TILE_SIZE - realPos.x, doublePoint.y / TILE_SIZE - realPos.y);

    ChunkData *chunk = this->getChunkOrGenerateIt(chunkX, chunkY);
    return chunk->isInside(sf::Vector2i(tileX, tileY), relativePoint, m_pValue);
}

void WorldData::addPointValue(sf::Vector2i position, float addValue)
{
    int chunkX;
    int chunkY;
    int tileX;
    int tileY;
    
    if (position.x >= 0) {
        chunkX = position.x / CHUNK_SIZE;
        tileX = position.x % CHUNK_SIZE;
    } else {
        chunkX = (position.x + 1) / CHUNK_SIZE - 1;
        tileX = CHUNK_SIZE + ((position.x + 1) % CHUNK_SIZE) - 1;
    }
    if (position.y >= 0) {
        chunkY = position.y / CHUNK_SIZE;
        tileY = position.y % CHUNK_SIZE;
    } else {
        chunkY = position.y / CHUNK_SIZE - 1;
        tileY = CHUNK_SIZE + ((position.y + 1) % CHUNK_SIZE) - 1;
    }

    ChunkData *chunk00 = this->getChunkOrGenerateIt(chunkX, chunkY);
    chunk00->addPointValue(sf::Vector2i(tileX, tileY), addValue);

    if (tileX == 0) {
        ChunkData *chunk10 = this->getChunkOrGenerateIt(chunkX-1, chunkY);
        chunk10->addPointValue(sf::Vector2i(CHUNK_SIZE, tileY), addValue);
    }
    if (tileY == 0) {
        ChunkData *chunk01 = this->getChunkOrGenerateIt(chunkX, chunkY-1);
        chunk01->addPointValue(sf::Vector2i(tileX, CHUNK_SIZE), addValue);
    }
    if (tileX == 0 && tileY == 0) {
        ChunkData *chunk11 = this->getChunkOrGenerateIt(chunkX-1, chunkY-1);
        chunk11->addPointValue(sf::Vector2i(CHUNK_SIZE, CHUNK_SIZE), addValue);
    }
}

void WorldData::generateChunk(int x, int y)
{
    m_worldGenerator->generateChunk(x, y);
    m_worldGenerator->generateChunk(x+1, y);
    m_worldGenerator->generateChunk(x+1, y+1);
    m_worldGenerator->generateChunk(x, y+1);
    MGChunk *dataChunk00 = m_worldGenerator->getChunk(x, y);
    MGChunk *dataChunk10 = m_worldGenerator->getChunk(x+1, y);
    MGChunk *dataChunk11 = m_worldGenerator->getChunk(x+1, y+1);
    MGChunk *dataChunk01 = m_worldGenerator->getChunk(x, y+1);
    
	ChunkData *chunkData = new ChunkData(this, sf::Vector2i(x, y));
	
	chunkData->setTerrain(
            dataChunk00->getTerrainLevel(), 
            dataChunk10->getTerrainLevel(), 
            dataChunk11->getTerrainLevel(), 
            dataChunk01->getTerrainLevel());

    m_chunks[this->chunkKey(x, y)] = chunkData;
}

ChunkData *WorldData::getChunkOrGenerateIt(int x, int y)
{
    //sf::Clock clock;
    std::string chunkKey = this->chunkKey(x, y);
    std::map<std::string, ChunkData *>::iterator it = m_chunks.find(chunkKey);

    if (it == m_chunks.end()) {
        m_worldGenerator->generateChunk(x, y);
        m_worldGenerator->generateChunk(x + 1, y);
        m_worldGenerator->generateChunk(x + 1, y + 1);
        m_worldGenerator->generateChunk(x, y + 1);
        float *dUL = m_worldGenerator->getChunk(x, y)->getTerrainLevel();
        float *dUR = m_worldGenerator->getChunk(x + 1, y)->getTerrainLevel();
        float *dDR = m_worldGenerator->getChunk(x + 1, y + 1)->getTerrainLevel();
        float *dDL = m_worldGenerator->getChunk(x, y + 1)->getTerrainLevel();
        ChunkData *chunkData = new ChunkData(this, sf::Vector2i(x, y));
        chunkData->setTerrain(dUL, dUR, dDR, dDL);
        m_chunks[chunkKey] = chunkData;
        return chunkData;
    } else {
        return it->second;
    }
}

std::string WorldData::chunkKey(int x, int y)
{
    return std::to_string(x) + ";" + std::to_string(y);
}