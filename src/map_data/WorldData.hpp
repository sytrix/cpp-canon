#ifndef WORLD_DATA_H
#define WORLD_DATA_H

#include <SFML/System/Vector2.hpp>
#include <map>
#include <string>
class MGWorld;
class ChunkData;

class WorldData
{
	public:
		WorldData(std::string seed, float pValue);
		~WorldData();

		void brush(sf::Vector2f position, float radius, float addValue);
		sf::Vector2f getCollisionPoint(sf::Vector2f pointA, sf::Vector2f pointB);
		sf::Vector2f getNearestPoint(sf::Vector2f point, float radius);
		bool isInsideWall(sf::Vector2f point);

		ChunkData *getChunkOrGenerateIt(int x, int y);
		void generateChunk(int x, int y);
		
	protected:
		
	private:

		void addPointValue(sf::Vector2i position, float addValue);
		std::string chunkKey(int x, int y);

		std::map<std::string, ChunkData*> m_chunks;
		MGWorld *m_worldGenerator;
		double m_pValue;
		
};

#endif
