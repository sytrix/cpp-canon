#include "ChunkData.hpp"

#include <iostream>
#include <cmath>
#include <cfloat>

#include "../utils/utils.hpp"
#include "../const.hpp"

ChunkData::ChunkData(WorldData *worldData, sf::Vector2i chunkPosition) 
: 
    m_worldData(worldData),
    m_chunkPosition(chunkPosition),
    m_isModified(false),
    m_isRender(false)
{
	m_data = new float[(CHUNK_SIZE + 1) * (CHUNK_SIZE + 1)];
}

ChunkData::~ChunkData() {
	delete m_data;
}

float *ChunkData::getData()
{
    return m_data;
}
bool ChunkData::isModified()
{
    return m_isModified;
}

bool ChunkData::isRender()
{
    return m_isRender;
}

void ChunkData::render()
{
    m_isRender = true;
}

void ChunkData::setTerrain(float *tUL, float *tUR, float *tDR, float *tDL)
{
    for (int x = 0; x < CHUNK_SIZE + 1; ++x) {
        if (x != CHUNK_SIZE) {
            for (int y = 0; y < CHUNK_SIZE + 1; ++y) {
                if (y == CHUNK_SIZE) {
                    m_data[y * (CHUNK_SIZE + 1) + x] = tDL[x];
                } else {
                    m_data[y * (CHUNK_SIZE + 1) + x] = tUL[y * CHUNK_SIZE + x];
                }
            }
        } else {
            for (int y = 0; y < CHUNK_SIZE + 1; ++y) {
                if (y == CHUNK_SIZE) {
                    m_data[y * (CHUNK_SIZE + 1) + x] = tDR[0];
                } else {
                    m_data[y * (CHUNK_SIZE + 1) + x] = tUR[y * CHUNK_SIZE];
                }
            }
        }
    }
}

void ChunkData::addPointValue(sf::Vector2i tilePosition, float addValue)
{
    m_data[tilePosition.y * (CHUNK_SIZE + 1) + tilePosition.x] += addValue;

    m_isModified = true;
    m_isRender = false;
}

sf::Vector2<double> ChunkData::getCollisionPoint(sf::Vector2i tilePos, sf::Vector2<double> pointA, sf::Vector2<double> pointB, double pValue)
{
    float vUL = m_data[tilePos.y * (CHUNK_SIZE + 1) + tilePos.x];
    float vDL = m_data[(tilePos.y + 1) * (CHUNK_SIZE + 1) + tilePos.x];
    float vDR = m_data[(tilePos.y + 1) * (CHUNK_SIZE + 1) + (tilePos.x + 1)];
    float vUR = m_data[tilePos.y * (CHUNK_SIZE + 1) + (tilePos.x + 1)];

    unsigned char code = getCode(vUL, vUR, vDR, vDL, pValue);

    if (code == 0x00 || code == 0x0F) {
        return sf::Vector2<double>(NAN, NAN);
    }

    sf::Vector2<double> tilePointA = getPointAWithCode(code, vUL, vUR, vDR, vDL, pValue);
    sf::Vector2<double> tilePointB = getPointBWithCode(code, vUL, vUR, vDR, vDL, pValue);

    sf::Vector3<double> cartA = equCart(pointA, pointB);
    sf::Vector3<double> cartB = equCart(tilePointA, tilePointB);

    sf::Vector2<double> pointInter = intersectionPoint(cartA, cartB);

    if (!isnanf(pointInter.x) && !isnanf(pointInter.y)) {
        if (checkPointBetween(pointInter, pointA, pointB, tilePointA, tilePointB)) {
            return pointInter;
        }
    }

    if (code == 0x0A || code == 0x05) {
        sf::Vector2<double> tilePointC = getPointCWithCode(code, vUL, vUR, vDR, vDL, pValue);
        sf::Vector2<double> tilePointD = getPointDWithCode(code, vUL, vUR, vDR, vDL, pValue);
        
        sf::Vector3<double> cartC = equCart(tilePointC, tilePointD);

        pointInter = intersectionPoint(cartA, cartC);

        if (!isnanf(pointInter.x) && !isnanf(pointInter.y)) {
            if (checkPointBetween(pointInter, pointA, pointB, tilePointC, tilePointD)) {
                return pointInter;
            }
        }
    }

    return sf::Vector2<double>(NAN, NAN);
}

sf::Vector2<double> ChunkData::getNearestPoint(sf::Vector2i tilePos, sf::Vector2<double> doublePoint, double pValue)
{
    float vUL = m_data[tilePos.y * (CHUNK_SIZE + 1) + tilePos.x];
    float vDL = m_data[(tilePos.y + 1) * (CHUNK_SIZE + 1) + tilePos.x];
    float vDR = m_data[(tilePos.y + 1) * (CHUNK_SIZE + 1) + (tilePos.x + 1)];
    float vUR = m_data[tilePos.y * (CHUNK_SIZE + 1) + (tilePos.x + 1)];

    unsigned char code = getCode(vUL, vUR, vDR, vDL, pValue);

    //std::cout << "coord:" << std::to_string(tilePos.x) << "," << std::to_string(tilePos.y) << " - code:" << std::to_string(code) << std::endl;

    if (code == 0x00 || code == 0x0F) {
        return sf::Vector2<double>(NAN, NAN);
    }

    sf::Vector2<double> tilePointA = getPointAWithCode(code, vUL, vUR, vDR, vDL, pValue);
    sf::Vector2<double> tilePointB = getPointBWithCode(code, vUL, vUR, vDR, vDL, pValue);

    sf::Vector2<double> nearestPointAB = pointDistanceMinSegment(tilePointA, tilePointB, doublePoint);
    //std::cout << "coord:" << std::to_string(tilePos.x) << "," << std::to_string(tilePos.y) << " - code:" << std::to_string(code) << " - nearA:" << std::to_string(nearestPointA.x) << "," << std::to_string(nearestPointA.y) << std::endl;

    if (code == 0x0A || code == 0x05) {
        sf::Vector2<double> tilePointC = getPointCWithCode(code, vUL, vUR, vDR, vDL, pValue);
        sf::Vector2<double> tilePointD = getPointDWithCode(code, vUL, vUR, vDR, vDL, pValue);
        
        sf::Vector2<double> nearestPointCD = pointDistanceMinSegment(tilePointC, tilePointD, doublePoint);

        if (!isnanf(nearestPointAB.x) && !isnanf(nearestPointAB.y)) {
            if (!isnanf(nearestPointCD.x) && !isnanf(nearestPointCD.y)) {
                double distA = distance(nearestPointAB, doublePoint);
                double distB = distance(nearestPointCD, doublePoint);

                if (distA < distB) {
                    return nearestPointAB;
                } else {
                    return nearestPointCD;
                }
            } else {
                return sf::Vector2<double>(NAN, NAN);
            }
        } else {
            return nearestPointCD;
        }


        
    }

    return nearestPointAB;
}

bool ChunkData::isInside(sf::Vector2i tilePos, sf::Vector2<double> relativePoint, double pValue)
{
    float vUL = m_data[tilePos.y * (CHUNK_SIZE + 1) + tilePos.x];
    float vDL = m_data[(tilePos.y + 1) * (CHUNK_SIZE + 1) + tilePos.x];
    float vDR = m_data[(tilePos.y + 1) * (CHUNK_SIZE + 1) + (tilePos.x + 1)];
    float vUR = m_data[tilePos.y * (CHUNK_SIZE + 1) + (tilePos.x + 1)];

    unsigned char code = getCode(vUL, vUR, vDR, vDL, pValue);

    if (code == 0x00) {
        return false;
    }
    if (code == 0x0F) {
        return true;
    }

    sf::Vector2<double> tilePointA = getPointAWithCode(code, vUL, vUR, vDR, vDL, pValue);
    sf::Vector2<double> tilePointB = getPointBWithCode(code, vUL, vUR, vDR, vDL, pValue);
    sf::Vector2<double> tilePointC = getPointCWithCode(code, vUL, vUR, vDR, vDL, pValue);
    sf::Vector2<double> tilePointD = getPointDWithCode(code, vUL, vUR, vDR, vDL, pValue);

    if (code == 0x0E)       { return isInsidePenta(tilePointA, sf::Vector2<double>(1.0, 0.0), sf::Vector2<double>(1.0, 1.0), sf::Vector2<double>(0.0, 1.0), tilePointB, relativePoint);}
    else if (code == 0x0D)  { return isInsidePenta(tilePointA, tilePointB, sf::Vector2<double>(1.0, 1.0), sf::Vector2<double>(0.0, 1.0), sf::Vector2<double>(0.0, 0.0), relativePoint);}
    else if (code == 0x0C)  { return isInsideQuad(tilePointA, tilePointB, sf::Vector2<double>(1.0, 1.0), sf::Vector2<double>(0.0, 1.0), relativePoint);}
    else if (code == 0x0B)  { return isInsidePenta(sf::Vector2<double>(0.0, 0.0), sf::Vector2<double>(1.0, 0.0), tilePointA, tilePointB, sf::Vector2<double>(0.0, 1.0), relativePoint);}
    else if (code == 0x0A)  { return isInsideTriangle(tilePointA, sf::Vector2<double>(1.0, 0.0), tilePointB, relativePoint) 
                                    || isInsideTriangle(tilePointC, tilePointD, sf::Vector2<double>(0.0, 1.0), relativePoint);}
    else if (code == 0x09)  { return isInsideQuad(sf::Vector2<double>(0.0, 0.0), tilePointA, tilePointB, sf::Vector2<double>(0.0, 1.0), relativePoint);}
    else if (code == 0x08)  { return isInsideTriangle(tilePointA, tilePointB, sf::Vector2<double>(0.0, 1.0), relativePoint);}
    else if (code == 0x07)  { return isInsidePenta(sf::Vector2<double>(0.0, 0.0), sf::Vector2<double>(1.0, 0.0), sf::Vector2<double>(1.0, 1.0), tilePointA, tilePointB, relativePoint);}
    else if (code == 0x06)  { return isInsideQuad(tilePointA, sf::Vector2<double>(1.0, 0.0), sf::Vector2<double>(1.0, 1.0), tilePointB, relativePoint);}
    else if (code == 0x05)  { return isInsideTriangle(sf::Vector2<double>(0.0, 0.0), tilePointA, tilePointB, relativePoint) 
                                    || isInsideTriangle(tilePointD, sf::Vector2<double>(1.0, 1.0), tilePointC, relativePoint);}
    else if (code == 0x04)  { return isInsideTriangle(tilePointA, sf::Vector2<double>(1.0, 1.0), tilePointB, relativePoint);}
    else if (code == 0x03)  { return isInsideQuad(sf::Vector2<double>(0.0, 0.0), sf::Vector2<double>(1.0, 0.0), tilePointA, tilePointB, relativePoint);}
    else if (code == 0x02)  { return isInsideTriangle(tilePointA, sf::Vector2<double>(1.0, 0.0), tilePointB, relativePoint);}
    else if (code == 0x01)  { return isInsideTriangle(sf::Vector2<double>(0.0, 0.0), tilePointA, tilePointB, relativePoint);}

    return false;
}

