#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "graphic/BulletView.hpp"
#include "graphic/TurretView.hpp"
#include "graphic/PolygoneView.hpp"
#include "graphic/RectangleView.hpp"
#include "graphic/GameManager.hpp"

#include "utils/TimeAnimation.hpp"

int main()
{
	srand(time(0));

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(1600, 1200), "SFML window");
	window.setFramerateLimit(60);

	RectangleView *rectangle1 = new RectangleView();
	rectangle1->setPosition(sf::Vector2f(100, 100));
	rectangle1->setSize(sf::Vector2f(200, 200));
	rectangle1->setColor(sf::Color(220, 90, 70));
	RectangleView *rectangle2 = new RectangleView();
	rectangle2->setPosition(sf::Vector2f(150, 500));
	rectangle2->setSize(sf::Vector2f(100, 200));
	rectangle2->setColor(sf::Color(220, 90, 70));
	RectangleView *rectangle3 = new RectangleView();
	rectangle3->setPosition(sf::Vector2f(800, 100));
	rectangle3->setSize(sf::Vector2f(80, 500));
	rectangle3->setColor(sf::Color(220, 90, 70));
	RectangleView *rectangle4 = new RectangleView();
	rectangle4->setPosition(sf::Vector2f(500, 800));
	rectangle4->setSize(sf::Vector2f(50, 50));
	rectangle4->setColor(sf::Color(220, 90, 70));
	RectangleView *rectangle5 = new RectangleView();
	rectangle5->setPosition(sf::Vector2f(120, 900));
	rectangle5->setSize(sf::Vector2f(600, 70));
	rectangle5->setColor(sf::Color(220, 90, 70));
	RectangleView *rectangle6 = new RectangleView();
	rectangle6->setPosition(sf::Vector2f(1000, 400));
	rectangle6->setSize(sf::Vector2f(150, 700));
	rectangle6->setColor(sf::Color(220, 90, 70));

	TurretView *canon1 = new TurretView();
	canon1->setPosition(sf::Vector2f(300, 200));
	// TurretView *canon2 = new TurretView();
	// canon2->setPosition(sf::Vector2f(800, 400));

	PolygoneView *polygone1 = new PolygoneView();
	polygone1->append(sf::Vertex(sf::Vector2f(488, 674), sf::Color(220, 90, 70)));
	polygone1->append(sf::Vertex(sf::Vector2f(447, 605), sf::Color(220, 90, 70)));
	polygone1->append(sf::Vertex(sf::Vector2f(510, 548), sf::Color(220, 90, 70)));
	polygone1->append(sf::Vertex(sf::Vector2f(684, 605), sf::Color(220, 90, 70)));
	polygone1->append(sf::Vertex(sf::Vector2f(618, 676), sf::Color(220, 90, 70)));

	PolygoneView *polygone2 = new PolygoneView();
	polygone2->append(sf::Vertex(sf::Vector2f(440,222), sf::Color(220, 90, 70))); 
	polygone2->append(sf::Vertex(sf::Vector2f(443,152), sf::Color(220, 90, 70))); 
	polygone2->append(sf::Vertex(sf::Vector2f(509,100), sf::Color(220, 90, 70))); 
	polygone2->append(sf::Vertex(sf::Vector2f(586,149), sf::Color(220, 90, 70))); 
	polygone2->append(sf::Vertex(sf::Vector2f(635,264), sf::Color(220, 90, 70))); 
	polygone2->append(sf::Vertex(sf::Vector2f(537,349), sf::Color(220, 90, 70)));

	GameManager gameManager;
	gameManager.addPolygone(polygone1);
	gameManager.addPolygone(polygone2);
	gameManager.addPolygone(rectangle1);
	gameManager.addPolygone(rectangle2);
	gameManager.addPolygone(rectangle3);
	gameManager.addPolygone(rectangle4);
	gameManager.addPolygone(rectangle5);
	gameManager.addPolygone(rectangle6);
	gameManager.addCanon(canon1);
	// gameManager.addCanon(canon2);

	sf::View view = window.getView();

	TimeAnimation animation;
	sf::Vector2f previousPosition = canon1->getPosition();
	sf::Vector2f nextPosition = canon1->getPosition();

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					gameManager.fire();
				}
			} else if (event.type == sf::Event::MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Button::Right) {
					//std::cout << "click coordinate : " << event.mouseButton.x << "," << event.mouseButton.y << std::endl;
					gameManager.fire();
				}
			} else if (event.type == sf::Event::MouseMoved) {
				gameManager.setMousePosition(sf::Vector2f(event.mouseMove.x, event.mouseMove.y) + (view.getCenter() - view.getSize() / 2.f));
			}
		}

		if (canon1->getPosition() != nextPosition) {

			if (animation.isRunning()) {
				previousPosition = animation.getCosInterpolation(previousPosition, nextPosition);
				nextPosition = canon1->getPosition();
			} else {
				previousPosition = nextPosition;
				nextPosition = canon1->getPosition();
			}

			animation.makeAnimation(0.5f);
		}

		sf::Vector2f transition = animation.getCosInterpolation(previousPosition, nextPosition);
		view.setCenter(transition);
		window.setView(view);

		gameManager.update();

		window.clear(sf::Color::Black);
		window.draw(gameManager);
		window.display();
	}

	return EXIT_SUCCESS;
}

