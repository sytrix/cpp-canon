#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "map_generator/MGWorld.hpp"
#include "map_generator/MGChunk.hpp"
#include "map_generator/MGLayerConfig.hpp"

#include "graphic/TurretView.hpp"
#include "graphic/GameManager.hpp"
#include "graphic/WorldView.hpp"

#include "utils/TimeAnimation.hpp"
#include "utils/utils.hpp"

int main()
{
	// Create the main window
	unsigned int width = sf::VideoMode::getDesktopMode().width;
	unsigned int height = sf::VideoMode::getDesktopMode().height;
	sf::RenderWindow window(sf::VideoMode(width, height), "SFML window");
	window.setFramerateLimit(60);

#if defined(WIN32) || defined(_WIN64)
	window.setPosition(sf::Vector2i(-8, 0));
#endif

	float pValue = 0.75f;
	GameManager gameManager("bob", pValue);

	TurretView *canon = new TurretView();
	canon->setPosition(sf::Vector2f(300, 200));
	gameManager.addCanon(canon);

	sf::View view = window.getView();

	sf::Vector2f previousPosition;
	sf::Vector2f nextPosition;

	TimeAnimation animation;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					gameManager.fire();
				}
				else if (event.key.code == sf::Keyboard::Escape)
				{
					window.close();
				}
			} else if (event.type == sf::Event::KeyReleased) {
				
			} else if (event.type == sf::Event::MouseButtonPressed) {

				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					
				} else if (event.mouseButton.button == sf::Mouse::Button::Right) {
					gameManager.fire();

				}

			} else if (event.type == sf::Event::MouseButtonReleased) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					
				} else if (event.mouseButton.button == sf::Mouse::Button::Right) {
					
				}
			} else if (event.type == sf::Event::MouseMoved) {
				gameManager.setMousePosition(
					sf::Vector2f(event.mouseMove.x, event.mouseMove.y) + (view.getCenter() - view.getSize() / 2.f));
			}
		}

		gameManager.update2();

		// smooth camera movement
		if (canon->getPosition() != nextPosition) {

			if (animation.isRunning()) {
				previousPosition = animation.getCosInterpolation(previousPosition, nextPosition);
				nextPosition = canon->getPosition();
			} else {
				previousPosition = nextPosition;
				nextPosition = canon->getPosition();
			}

			animation.makeAnimation(0.5f);
		}
		sf::Vector2f transition = animation.getCosInterpolation(previousPosition, nextPosition);
		view.setCenter(transition);

		// update display depending the area that have to be display
		gameManager.setView(view);
		window.setView(view);

		// draw
		window.clear(sf::Color(0, 0, 0));
		window.draw(gameManager);
		window.display();
	}

	

	return 0;
}

