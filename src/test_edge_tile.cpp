#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "graphic/TileView.hpp"

int main()
{
	srand(time(0));

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(1600, 1200), "SFML window");
	window.setFramerateLimit(60);


	TileView **tiles = new TileView*[16];
	for (int i = 0; i < 16; ++i) {
		tiles[i] = new TileView(
			0, 30 - (i * 2), 
			(float)(int)((i & 0x01)), 
			(float)(int)((i & 0x02) >> 1), 
			(float)(int)((i & 0x04) >> 2), 
			(float)(int)((i & 0x08) >> 3), 
			0.2f);
	}

	int mapLenX = 10;
	int mapLenY = 10;

	float **map = new float*[mapLenX];
	for (int x = 0; x < mapLenX; ++x) {
		map[x] = new float[mapLenY];
		for (int y = 0; y < mapLenY; ++y) {
			map[x][y] = (float)(rand() % 1000) / 1000.f;
		}
	}


	TileView ***mapTiles = new TileView**[mapLenX];
	for (int x = 0; x < mapLenX - 1; ++x) {
		mapTiles[x] = new TileView*[mapLenX];
		for (int y = 0; y < mapLenY - 1; ++y) {
			mapTiles[x][y] = new TileView(x + 3, y, map[x][y], map[x + 1][y], map[x + 1][y + 1], map[x][y + 1], 0.5f);
		}
	}
	

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					
				}
			} else if (event.type == sf::Event::MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					
				}
			} else if (event.type == sf::Event::MouseMoved) {
			}
		}

		window.clear(sf::Color::Black);
		for (int i = 0; i < 16; ++i) {
			window.draw(*(tiles[i]));
		}
		for (int x = 0; x < mapLenX - 1; ++x) {
			for (int y = 0; y < mapLenY - 1; ++y) {
				window.draw(*(mapTiles[x][y]));
			}
		}
		window.display();
	}

	for (int x = 0; x < mapLenX - 1; ++x) {
		for (int y = 0; y < mapLenY - 1; ++y) {
			delete mapTiles[x][y];
		}
		delete mapTiles[x];
	}
	delete mapTiles;

	for (int i = 0; i < 16; ++i) {
		delete tiles[i];
	}
	delete tiles;

	return EXIT_SUCCESS;
}

