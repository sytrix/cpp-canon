#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <cmath>

#include "graphic/WorldView.hpp"

int main()
{
	srand(time(0));

	// Create the main window
	sf::RenderWindow window(sf::VideoMode(2000, 1200), "SFML window");
	window.setFramerateLimit(60);

	float pValue = 0.75f;
	WorldView world("bob", pValue);

	world.generateChunk(0, 0);
	world.generateChunk(1, 0);
	world.generateChunk(1, 1);
	world.generateChunk(0, 1);

	sf::View originalView = window.getView();

	const float movementSpeed = 12.f;
	const float rotationSpeed = 0.2f;
	const float zoomSpeed = 0.02f;
	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;
	bool turnLeft = false;
	bool turnRight = false;
	bool zoomIn = false;
	bool zoomOut = false;

	float zoom = 1.f;

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed) {
				window.close();
			} else if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Space) {
					
				} else if (event.key.code == sf::Keyboard::R) {
					pValue += 0.01f;
					if (pValue > 1.f) {
						pValue = 1.f;
					}
					world.setPValue(pValue);
				} else if (event.key.code == sf::Keyboard::F) {
					pValue -= 0.01f;
					if (pValue < 0.f) {
						pValue = 0.f;
					}
					world.setPValue(pValue);
				} else if (event.key.code == sf::Keyboard::Z) {
					up = true;
				} else if (event.key.code == sf::Keyboard::S) {
					down = true;
				} else if (event.key.code == sf::Keyboard::Q) {
					left = true;
				} else if (event.key.code == sf::Keyboard::D) {
					right = true;
				} else if (event.key.code == sf::Keyboard::A) {
					turnLeft = true;
				} else if (event.key.code == sf::Keyboard::E) {
					turnRight = true;
				} else if (event.key.code == sf::Keyboard::T) {
					zoomIn = true;
				} else if (event.key.code == sf::Keyboard::G) {
					zoomOut = true;
				}
			} else if (event.type == sf::Event::KeyReleased) {
				if (event.key.code == sf::Keyboard::Z) {
					up = false;
				} else if (event.key.code == sf::Keyboard::S) {
					down = false;
				} else if (event.key.code == sf::Keyboard::Q) {
					left = false;
				} else if (event.key.code == sf::Keyboard::D) {
					right = false;
				} else if (event.key.code == sf::Keyboard::A) {
					turnLeft = false;
				} else if (event.key.code == sf::Keyboard::E) {
					turnRight = false;
				} else if (event.key.code == sf::Keyboard::T) {
					zoomIn = false;
				} else if (event.key.code == sf::Keyboard::G) {
					zoomOut = false;
				}
			} else if (event.type == sf::Event::MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					std::cout << "click coordinate : " << event.mouseButton.x << "," << event.mouseButton.y << std::endl;
				}
			} else if (event.type == sf::Event::MouseMoved) {
				//gameManager.setMousePosition(sf::Vector2f(event.mouseMove.x, event.mouseMove.y));
			}
		}

		if (up) {
			originalView.move(sf::Vector2f(0, -movementSpeed));
		}
		if (down) {
			originalView.move(sf::Vector2f(0, movementSpeed));
		}
		if (left) {
			originalView.move(sf::Vector2f(-movementSpeed, 0));
		}
		if (right) {
			originalView.move(sf::Vector2f(movementSpeed, 0));
		}
		if (turnLeft) {
			originalView.rotate(-rotationSpeed);
		}
		if (turnRight) {
			originalView.rotate(rotationSpeed);
		}
		if (zoomIn) {
			originalView.zoom(1.f - zoomSpeed);
		}
		if (zoomOut) {
			originalView.zoom(1.f + zoomSpeed);
		}

		window.setView(originalView);

		window.clear(sf::Color::Black);
		window.draw(world);
		window.display();
	}

	

	return EXIT_SUCCESS;
}

